=====
Usage
=====

To use Django Courses Frontend in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'courses_frontend.apps.CoursesFrontendConfig',
        ...
    )

Add Django Courses Frontend's URL patterns:

.. code-block:: python

    from courses_frontend import urls as courses_frontend_urls


    urlpatterns = [
        ...
        url(r'^', include(courses_frontend_urls)),
        ...
    ]
