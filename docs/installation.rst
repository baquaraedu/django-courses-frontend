============
Installation
============

At the command line::

    $ easy_install django-courses-frontend

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-courses-frontend
    $ pip install django-courses-frontend
