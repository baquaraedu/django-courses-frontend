=============================
Django Courses Frontend
=============================

.. image:: https://badge.fury.io/py/django-courses-frontend.svg
    :target: https://badge.fury.io/py/django-courses-frontend

.. image:: https://travis-ci.org/brunosmartin/django-courses-frontend.svg?branch=master
    :target: https://travis-ci.org/brunosmartin/django-courses-frontend

.. image:: https://codecov.io/gh/brunosmartin/django-courses-frontend/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/brunosmartin/django-courses-frontend

AngularJS frontend for Django Courses (Baquara)

Documentation
-------------

The full documentation is at https://django-courses-frontend.readthedocs.io.

Quickstart
----------

Install Django Courses Frontend::

    pip install django-courses-frontend

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'courses_frontend.apps.CoursesFrontendConfig',
        ...
    )

Add Django Courses Frontend's URL patterns:

.. code-block:: python

    from courses_frontend import urls as courses_frontend_urls


    urlpatterns = [
        ...
        url(r'^', include(courses_frontend_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox


Development commands
---------------------

::

    pip install -r requirements_dev.txt
    invoke -l


Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
