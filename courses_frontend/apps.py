# -*- coding: utf-8
from django.apps import AppConfig


class CoursesFrontendConfig(AppConfig):
    name = 'courses_frontend'
