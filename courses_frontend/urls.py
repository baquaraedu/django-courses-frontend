from django.conf.urls import include, url
from django.views.generic import TemplateView

from . import views


app_name = 'courses_frontend'
urlpatterns = [
    url(r'^$', views.base_spa_view, name='base_spa'),
    url(r'^header.template.html', TemplateView.as_view(template_name="components/header.html"), name='header'),
    url(r'^header-unlogged.template.html', TemplateView.as_view(template_name="components/header-unlogged.html"), name='header-unlogged'),
    url(r'^main-nav.template.html', TemplateView.as_view(template_name="components/main-nav.html"), name='main-nav'),
    url(r'^home.template.html', TemplateView.as_view(template_name="components/home.html"), name='home'),
    url(r'^dashboard.template.html', TemplateView.as_view(template_name="components/dashboard.html"), name='dashboard'),
    url(r'^api/i18n/', include('django.conf.urls.i18n')),

    # Courses
    url(r'^courses.template.html', TemplateView.as_view(template_name="components/courses/courses.html"), name='courses'),
    url(r'^course-card.template.html', TemplateView.as_view(template_name="components/courses/course-card.html"), name='course-card'),
    url(r'^course.template.html', TemplateView.as_view(template_name="components/courses/course.html"), name='course'),
    url(r'^course-header-inline.partial.html', TemplateView.as_view(template_name="components/courses/course-header-inline.html"), name='course'),

    # Forums
    url(r'^forum.template.html', TemplateView.as_view(template_name="components/forum.html"), name='forum'),
    url(r'^topic.template.html', TemplateView.as_view(template_name="components/topic.html"), name='topic'),
    url(r'^profile.template.html', TemplateView.as_view(template_name="components/profile.html"), name='profile'),
    url(r'^new-topic.template.html', TemplateView.as_view(template_name="components/new-topic.html"), name='new-topic'),

    # Category
    url(r'^category-edit.template.html', TemplateView.as_view(template_name="components/category/category-edit.html"), name='category-edit'),
    url(r'^category-list.template.html', TemplateView.as_view(template_name="components/category/category-list.html"), name='category-list'),

    # Themes
    url(r'^theme-edit.template.html', TemplateView.as_view(template_name="components/theme/theme-edit.html"), name='theme-edit'),
    url(r'^theme-list.template.html', TemplateView.as_view(template_name="components/theme/theme-list.html"), name='theme-list'),

    # Themes
    url(r'^format-edit.template.html', TemplateView.as_view(template_name="components/format/format-edit.html"), name='format-edit'),
    url(r'^format-list.template.html', TemplateView.as_view(template_name="components/format/format-list.html"), name='format-list'),

    # Cards
    url(r'^cards-list.template.html', TemplateView.as_view(template_name="components/cards/cards-list.html"), name='cards-list'),
    url(r'^card.template.html', TemplateView.as_view(template_name="components/cards/card.html"), name='card'),
    url(r'^card-edit.template.html', TemplateView.as_view(template_name="components/cards/card-edit.html"), name='card-edit'),
    url(r'^card-detail.template.html', TemplateView.as_view(template_name="components/cards/card-detail.html"), name='card-detail'),
    url(r'^cards-carousel.template.html', TemplateView.as_view(template_name="components/cards/cards-carousel.html"), name='cards-carousel'),

    # Events
    url(r'^events-dashboard.template.html', TemplateView.as_view(template_name="components/events/events-dashboard.html"), name='events-dashboard'),
    url(r'^events-list.template.html', TemplateView.as_view(template_name='components/events/events-list.html'), name='events-list'),

    # Reports
    url(r'^general-reports.template.html', TemplateView.as_view(template_name="components/reports/general-reports.html"), name='general-reports'),
    url(r'^user-reports.template.html', TemplateView.as_view(template_name="components/reports/user-reports.html"), name='user-reports'),
    url(r'^single-user-report.template.html', TemplateView.as_view(template_name="components/reports/user-report.html"), name='user-report'),
    url(r'^user-access-report.template.html', TemplateView.as_view(template_name="components/reports/user-access.html"), name='user-access-report'),
    url(r'^user-actions-report.template.html', TemplateView.as_view(template_name="components/reports/user-actions.html"), name='user-actions-report'),
    url(r'^user-course-report-list.template.html', TemplateView.as_view(template_name="components/reports/user-course-list.html"), name='user-course-report-list'),
    url(r'^user-course-report.template.html', TemplateView.as_view(template_name="components/reports/user-course.html"), name='user-course-report'),

    # Messages
    url(r'^messages.template.html', TemplateView.as_view(template_name="components/messages/messages.html"), name='messages'),
    url(r'^messages-list.template.html', TemplateView.as_view(template_name="components/messages/messages-list.html"), name='messages-list'),
    url(r'^messages-edit.template.html', TemplateView.as_view(template_name="components/messages/messages-edit.html"), name='messages-edit'),
    url(r'^messages-detail.template.html', TemplateView.as_view(template_name="components/messages/messages-detail.html"), name='messages-detail'),

    # i18n
    url(r'^edit-language-switcher.template.html', TemplateView.as_view(template_name="components/i18n/edit-language-switcher.html"), name='edit-language-switcher'),
]
