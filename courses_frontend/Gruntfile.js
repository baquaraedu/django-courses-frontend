module.exports = function(grunt) {
  grunt.initConfig({
    nggettext_extract: {
      pot: {
        files: {
          'po/template.pot': ['templates/components/*.html']
        }
      },
    },
    nggettext_compile: {
      all: {
        options: {
          module: 'baquara'
        },
        files: {
          'static/js/app/translations.js': ['po/*.po']
        }
      },
    },
  });

  grunt.registerTask('extract', ['nggettext_extract']);
  grunt.registerTask('compile', ['nggettext_compile']);

  // Load up tasks
  grunt.loadNpmTasks('grunt-angular-gettext');
};
