from django.shortcuts import render
from django.contrib.auth.decorators import login_required


def base_spa_view(request, *args, **kwargs):
    return render(request, 'base_spa.html', {})
