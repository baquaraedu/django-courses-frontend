class CardDetailCtrl {
    constructor(
        $scope,
        $rootScope,
        $state,
        $stateParams,
        $sce,
        Cards,
        Likes,
        Audiences,
        Axes,
        Tags,
        CardFile,
        YouTubeEmbeds,
        UserActions,
        gettextCatalog,
        CardsHelpers
    ) {
        var ctrl = this;

        const fetchCard = () => {
            $scope.card_id = $stateParams.cardId;
            $scope.card = Cards.get({ id: $scope.card_id }, function (response) {
                $scope.card = response;
                const image_slides = ($scope.card.image_gallery || []).map(function (img) {
                    return { type: 'image', el: img, card_slide_index: img.card_slide_index };
                });
                const video_slides = ($scope.card.youtube_embeds || []).map(function (video) {
                    return { type: 'video', el: video, card_slide_index: video.card_slide_index };
                });
                $scope.slides = [...image_slides, ...video_slides].sort(CardsHelpers.sort_slides_by_index);
            });
        }

        ctrl.$onInit = function () {
            fetchCard();

            $scope.$on('languageChanged', () => {
                fetchCard();
            });

            // (new UserActions({
            //     verb: "access",
            //     action_object_id: $stateParams.cardId,
            //     action_object_type: "Card",
            //     target_id: $stateParams.classroomId,
            //     target_type: "Classroom"
            //   })).$save();
        };

        $scope.toggle_like = function () {
            if ($scope.card.user_liked) {
                Likes.delete({ id: $scope.card.user_liked }).$promise.then(function () {
                    $scope.card.user_liked = null;
                    $scope.card.likes -= 1;
                });
            }
            else {
                Likes.save({ card: $scope.card.id }).$promise.then(function (response) {
                    $scope.card.user_liked = response.id;
                    $scope.card.likes += 1;
                });
                (new UserActions({
                    verb: "reacted",
                    action_object_id: $stateParams.cardId,
                    action_object_type: "Card",
                    target_id: $stateParams.classroomId,
                    target_type: "Classroom"
                })).$save();
            }
        };

        function start_rootscope() {
            $rootScope.card_filter = {};
            $rootScope.card_filter.keyword = '';
            $rootScope.card_filter.audience = '';
            $rootScope.card_filter.axis = '';
            $rootScope.card_filter.status = '';
            $rootScope.card_filter.tags = [];
        }

        $scope.filter_by_audience = function (audience) {
            if (!$rootScope.card_filter)
                start_rootscope();
            $rootScope.card_filter.audience = audience;
            window.location.replace('#!/');
        };
        $scope.filter_by_axis = function (axis) {
            if (!$rootScope.card_filter)
                start_rootscope();
            $rootScope.card_filter.axis = axis;
            window.location.replace('#!/');
        };
        $scope.filter_by_tag = function (tag) {
            if (!$rootScope.card_filter)
                start_rootscope();
            $rootScope.card_filter.tags.push({ name: tag });
            window.location.replace('#!/');
        };

        $scope.delete_card = function () {
            let text = gettextCatalog.getString('Do you want to exclude this card?');
            if (window.confirm(text)) {
                if ($scope.card.editable) {
                    Cards.delete({ id: $scope.card.id }).$promise.then(function (response) {
                        $state.go('cardsList');
                    }).catch(function (error) {
                        let text = gettextCatalog.getString('Unable to delete card.');
                        $scope.error_messages.push(text);
                        console.error(error);
                    });
                }
            }
        };

        /* Slider*/
        $scope.active_slide = 0;
        $scope.safe_url = function (url) {
            return $sce.trustAsResourceUrl(url);
        };
        $scope.safe_html = function (text) {
            return $sce.trustAsHtml(text);
        };
    }
}

CardDetailCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$stateParams',
    '$sce',
    'Cards',
    'Likes',
    'Audiences',
    'Axes',
    'Tags',
    'CardFile',
    'YouTubeEmbeds',
    'UserActions',
    'gettextCatalog',
    'CardsHelpers'
];

angular.
module('cardDetail').
    component('cardDetail', {
        templateUrl: '/card-detail.template.html',
        controller: CardDetailCtrl,
        bindings: {
            card: '<',
        },
});
