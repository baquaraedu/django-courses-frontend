(function() {
    'use strict';
    
    var app = angular.module('formatList', [
        'format-list.controllers',
        'ui.tinymce',
    ]);

})(angular);