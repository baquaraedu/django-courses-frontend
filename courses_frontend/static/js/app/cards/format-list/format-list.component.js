(function() {
    'use strict';

    angular.
    module('formatList').
    component('formatList', {
      templateUrl: '/format-list.template.html',
      controller: 'FormatListCtrl',
    });
})();
