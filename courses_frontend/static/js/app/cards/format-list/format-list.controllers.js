(function(angular){
    'use strict';

    var app = angular.module('format-list.controllers', ['ngSanitize']);
    
    app.controller('FormatListCtrl', ['$scope', 'AxesPage', 'UserLocalStorage',
        function ($scope, AxesPage, UserLocalStorage) {
            UserLocalStorage.set('editingLanguage', UserLocalStorage.get('currentLanguage'));
            $scope.format = {}
            $scope.search = ""
            
            // Pagination Params
            $scope.format.page_size = 20;
            $scope.current_page = 1;
            $scope.load_page = 1;
            $scope.format_page_loaded = false;

            $scope.formats = AxesPage.get({
                search: $scope.search,
                page: $scope.current_page,
                page_size: $scope.format.page_size,
            }, (response) => {
                $scope.formats = response.results
                $scope.format.total_format_items = response.count
                $scope.format.max_size = response.length
                $scope.format_page_loaded = response.$resolved
                $scope.format.has_next_page = (response.next !== null || response.previous !== null)
            }, (err) => {
                $scope.format_page_loaded = true
                $scope.formats = []
            });
            
            $scope.page_changed = function() {
                $scope.formats = AxesPage.get({
                    search: $scope.search,
                    page: $scope.current_page,
                    page_size: $scope.format.page_size,
                }, (response) => {
                    $scope.formats = response.results
                    $scope.format.total_format_items = response.count
                    $scope.format.max_size = response.length
                    $scope.format_page_loaded = response.$resolved;
                    $scope.format.has_next_page = (response.next !== null || response.previous !== null)
                }, (err) => {
                    $scope.format_page_loaded = true;
                    $scope.formats = []
                });
            };
        }
    ]);

})(window.angular);
