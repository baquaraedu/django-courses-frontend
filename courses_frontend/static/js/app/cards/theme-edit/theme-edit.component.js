(function() {
    'use strict';

    angular.
    module('themeEdit').
    component('themeEdit', {
      templateUrl: '/theme-edit.template.html',
      controller: 'ThemeEditCtrl',
    });
})();
