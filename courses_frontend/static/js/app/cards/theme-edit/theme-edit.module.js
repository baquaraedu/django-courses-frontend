(function() {
    'use strict';
    
    var app = angular.module('themeEdit', [
        'theme-edit.controllers',
        'ui.tinymce',
    ]);

})(angular);