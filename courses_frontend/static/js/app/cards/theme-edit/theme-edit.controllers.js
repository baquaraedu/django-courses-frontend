(function(angular){
    'use strict';

    var app = angular.module('theme-edit.controllers', ['ngSanitize']);
    
    app.controller('ThemeEditCtrl', ['$scope', '$window', '$stateParams', 'Audiences', 'SetLanguage','UserLocalStorage', 'gettextCatalog',
        function ($scope, $window, $stateParams, Audiences, SetLanguage, UserLocalStorage, gettextCatalog) {
            $scope.name = "";
            $scope.description = "";
            $scope.alert = {
                hidden: true,
                type: 'success'
            }

            if ($stateParams.themeId) {
                singleInit();
            }

            function singleInit(theme_id) {
                const themeId = $stateParams.themeId;

                if (theme_id) { themeId = theme_id; }

                Audiences.get({id: themeId}, (theme) => {
                    $scope.theme = theme;
                    $scope.name = theme.name;
                    $scope.description = theme.description;
                }, function(error) {
                    $scope.fatal_error = true;
                    $scope.error_message = error.data.message;
                });
            };

            $scope.editingLanguage = UserLocalStorage.get('editingLanguage') || UserLocalStorage.get('currentLanguage');
            
            $scope.setLanguage = function(language) {
                UserLocalStorage.set('editingLanguage', language);
                $scope.language = language;
                SetLanguage(language).then(function() {
                    $window.location.reload();
                });
            }

            $scope.delete_theme = function() {
                let theme = new Audiences();
                theme.id = $stateParams.themeId;
                theme.name = $scope.name;
                theme.description = $scope.description;

                if(!confirm(gettextCatalog.getString('Are you sure you want to remove this theme?'))) return;
                $scope.theme.$delete().then(function(){
                    window.location.href = `#!/themes/`;
                    $scope.alert = {
                        title: gettextCatalog.getString('Deleted with success'),
                        hidden: false,
                        type: 'success'
                    }
                });
            }

            $scope.save_theme = function() {
                let editingLanguage = $scope.editingLanguage;
                let theme = $scope.theme ?  $scope.theme : new Audiences();

                theme.id = $stateParams.themeId;
                theme.name = $scope.name;
                theme.description = $scope.description;

                if (editingLanguage === 'en') {
                    theme.name_en = theme.name;
                    theme.description_en = theme.description;
                } else if (editingLanguage === 'es') {
                    theme.name_es = theme.name;
                    theme.description_es = theme.description;
                } else if (editingLanguage === 'pt-br') {
                    theme.name_pt_br = theme.name;
                    theme.description_pt_br = theme.description;
                }

                if (theme.id) {
                    theme.$update(theme => {
                            $scope.theme = theme;
                            $scope.alert = {
                                title: gettextCatalog.getString('Updated with success'),
                                hidden: false,
                                type: 'success'
                            }
                        }, function(error) {
                            $scope.fatal_error = true;
                            $scope.alert = {
                                title: error.data.message,
                                hidden: false,
                                type: 'error'
                            }
                        }
                    )
                } else {
                    theme.$save(function(theme) {
                        window.location.href = `#!/theme/${theme.id}`;
                        $scope.alert = {
                            title: gettextCatalog.getString('Saved with success'),
                            hidden: false,
                            type: 'success'
                        }
                    })
                }
            }
        }
    ]);

})(window.angular);
