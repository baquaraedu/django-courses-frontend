(function() {
    'use strict';

    angular.
    module('formatEdit').
    component('formatEdit', {
      templateUrl: '/format-edit.template.html',
      controller: 'FormatEditCtrl',
    });
})();
