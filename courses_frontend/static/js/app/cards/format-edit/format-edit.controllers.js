(function(angular){
    'use strict';

    var app = angular.module('format-edit.controllers', ['ngSanitize']);
    
    app.controller('FormatEditCtrl', ['$scope', '$window', '$stateParams', 'Axes', 'SetLanguage','UserLocalStorage', 'gettextCatalog',
        function ($scope, $window, $stateParams, Axes, SetLanguage, UserLocalStorage, gettextCatalog) {
            $scope.name = "";
            $scope.description = "";
            $scope.alert = {
                hidden: true,
                type: 'success'
            }

            if ($stateParams.formatId) {
                singleInit();
            }

            function singleInit(format_id) {
                const formatId = $stateParams.formatId;

                if (format_id) { formatId = format_id; }

                Axes.get({id: formatId}, (format) => {
                    $scope.format = format;
                    $scope.name = format.name;
                    $scope.description = format.description;
                }, function(error) {
                    $scope.fatal_error = true;
                    $scope.error_message = error.data.message;
                });
            };

            $scope.editingLanguage = UserLocalStorage.get('editingLanguage') || UserLocalStorage.get('currentLanguage');
            
            $scope.setLanguage = function(language) {
                UserLocalStorage.set('editingLanguage', language);
                $scope.language = language;
                SetLanguage(language).then(function() {
                    $window.location.reload();
                });
            }

            $scope.delete_format = function() {
                let format = new Axes();
                format.id = $stateParams.formatId;
                format.name = $scope.name;
                format.description = $scope.description;

                if(!confirm(gettextCatalog.getString('Are you sure you want to remove this format?'))) return;
                $scope.format.$delete().then(function(){
                    window.location.href = `#!/formats/`;
                    $scope.alert = {
                        title: gettextCatalog.getString('Deleted with success'),
                        hidden: false,
                        type: 'success'
                    }
                });
            }

            $scope.save_format = function() {
                let editingLanguage = $scope.editingLanguage;
                let format = $scope.format ?  $scope.format : new Axes();

                format.id = $stateParams.formatId;
                format.name = $scope.name;
                format.description = $scope.description;

                if (editingLanguage === 'en') {
                    format.name_en = format.name;
                    format.description_en = format.description;
                } else if (editingLanguage === 'es') {
                    format.name_es = format.name;
                    format.description_es = format.description;
                } else if (editingLanguage === 'pt-br') {
                    format.name_pt_br = format.name;
                    format.description_pt_br = format.description;
                }

                if (format.id) {
                    format.$update(format => {
                            $scope.format = format;
                            $scope.alert = {
                                title: gettextCatalog.getString('Updated with success'),
                                hidden: false,
                                type: 'success'
                            }
                        }, function(error) {
                            $scope.fatal_error = true;
                            $scope.alert = {
                                title: error.data.message,
                                hidden: false,
                                type: 'error'
                            }
                        }
                    )
                } else {
                    format.$save(function(format) {
                        window.location.href = `#!/format/${format.id}`;
                        $scope.alert = {
                            title: gettextCatalog.getString('Saved with success'),
                            hidden: false,
                            type: 'success'
                        }
                    })
                }
            }
        }
    ]);

})(window.angular);
