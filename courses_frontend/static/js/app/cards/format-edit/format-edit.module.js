(function() {
    'use strict';
    
    var app = angular.module('formatEdit', [
        'format-edit.controllers',
        'ui.tinymce',
    ]);

})(angular);