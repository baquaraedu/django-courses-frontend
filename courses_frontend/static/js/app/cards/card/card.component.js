class CardCtrl {
    constructor(
        $scope,
        CardsHelpers
    ) {
        $scope.card_image = function (card) {
            const slides = [...card.image_gallery, ...card.youtube_embeds].sort(CardsHelpers.sort_slides_by_index);
            const first_slide = slides[0];
            if (first_slide) {
                if (first_slide.video_id) {
                    return `https://img.youtube.com/vi/${first_slide.video_id}/mqdefault.jpg`;
                } else if (first_slide.image) {
                    return first_slide.image;
                }
            }
            return '/static/img/card-default.png';
        };
    }
}

CardCtrl.$inject = [
    '$scope',
    'CardsHelpers'
]

angular.
    module('card').
        component('card', {
            templateUrl: '/card.template.html',
            controller: CardCtrl,
            bindings: {
                card: '<',
            },
        });
