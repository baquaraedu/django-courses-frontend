(function(angular){
    'use strict';
    var app = angular.module('cards.services', ['ngResource']);

    app.factory('Audiences', ['$resource', function($resource){
        return $resource('/cards/api/audience/:id',
            {'id': '@id'},
            {'update': {method: 'PUT'}});
    }]);

    app.factory('AudiencesPage', ['$resource', function($resource){
        return $resource('/cards/api/audiences');
    }]);

    app.factory('AxesPage', ['$resource', function($resource){
        return $resource('/cards/api/axes');
    }]);

    app.factory('Axes', ['$resource', function($resource){
        return $resource('/cards/api/axis/:id',
            {'id': '@id'},
            {'update': {method: 'PUT'}});
    }]);

    app.factory('Cards', ['$resource', function($resource){
        return $resource('/cards/api/cards/:id',
            {'id': '@id'},
            {'update': {method: 'PUT'}});
    }]);

    app.factory('Images', ['$resource', 'Upload', function($resource, Upload){
        var img_file = $resource('/cards/api/images/:id',
            {'id': '@id'});

        img_file.upload = function (file, { description = '', card_slide_index } = {}) {
            return Upload.upload({
                url: '/cards/api/images',
                data: {
                    name: file.name,
                    image: file,
                    description: description,
                    card_slide_index,
                },
                arrayKey: '',
            });
        };
        return img_file;
    }]);

    app.factory('CardFile', ['$resource', 'Upload', function($resource, Upload){
        var card_file = $resource('/cards/api/card-file/:id',
           {'id' : '@id'},
           {
               'update': {'method': 'PUT'},
               'patch': {'method': 'PATCH'}
           });

        card_file.upload = function(file) {
           return Upload.upload({
               url: '/cards/api/card-file',
               data: {
                   name: file.name,
                   file: file
               },
               arrayKey: '',
           });
        };
        return card_file;
   }]);

    app.factory('Likes', ['$resource', function($resource){
        return $resource('/cards/api/likes/:id',
            {'id': '@id'});
    }]);

    app.factory('Tags', ['$resource', function($resource){
        return $resource('/cards/api/tags/:id',
            {'id': '@id'});
    }]);

    app.factory('YouTubeEmbeds', ['$resource', function($resource){
        return $resource('/cards/api/youtube_embeds/:id',
            {'id': '@id'});
    }]);

    class CardsHelpers {
        constructor() {}

        sort_slides_by_index (a, b) {
            if (a.card_slide_index == null) {
                if (b.card_slide_index == null) {
                    return 0;
                } else {
                    return -1;
                }
            } else if (b.card_slide_index == null) {
                return 1;
            }
    
            if (a.card_slide_index > b.card_slide_index) {
                return 1;
            } else if (a.card_slide_index < b.card_slide_index) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    CardsHelpers.$inject = []

    app.service('CardsHelpers', CardsHelpers)

})(window.angular);
