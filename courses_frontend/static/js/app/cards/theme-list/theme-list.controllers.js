(function(angular){
    'use strict';

    var app = angular.module('theme-list.controllers', ['ngSanitize']);
    
    app.controller('ThemeListCtrl', ['$scope', 'AudiencesPage', 'UserLocalStorage',
        function ($scope, AudiencesPage, UserLocalStorage) {
            UserLocalStorage.set('editingLanguage', UserLocalStorage.get('currentLanguage'));
            $scope.theme = {}
            $scope.search = ""
            
            // Pagination Params
            $scope.theme.page_size = 20;
            $scope.current_page = 1;
            $scope.load_page = 1;
            $scope.theme_page_loaded = false;

            $scope.themes = AudiencesPage.get({
                search: $scope.search,
                page: $scope.current_page,
                page_size: $scope.theme.page_size,
            }, (response) => {
                $scope.themes = response.results
                $scope.theme.total_theme_items = response.count
                $scope.theme.max_size = response.length
                $scope.theme_page_loaded = response.$resolved
                $scope.theme.has_next_page = (response.next !== null || response.previous !== null)
            }, (err) => {
                $scope.theme_page_loaded = true
                $scope.themes = []
            });
            
            $scope.page_changed = function() {
                $scope.themes = AudiencesPage.get({
                    search: $scope.search,
                    page: $scope.current_page,
                    page_size: $scope.theme.page_size,
                }, (response) => {
                    $scope.themes = response.results
                    $scope.theme.total_theme_items = response.count
                    $scope.theme.max_size = response.length
                    $scope.theme_page_loaded = response.$resolved;
                    $scope.theme.has_next_page = (response.next !== null || response.previous !== null)
                }, (err) => {
                    $scope.theme_page_loaded = true;
                    $scope.themes = []
                });
            };
        }
    ]);

})(window.angular);
