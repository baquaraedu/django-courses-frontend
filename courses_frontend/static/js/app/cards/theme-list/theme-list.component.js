(function() {
    'use strict';

    angular.
    module('themeList').
    component('themeList', {
      templateUrl: '/theme-list.template.html',
      controller: 'ThemeListCtrl',
    });
})();
