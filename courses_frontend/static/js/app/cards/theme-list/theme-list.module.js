(function() {
    'use strict';
    
    var app = angular.module('themeList', [
        'theme-list.controllers',
        'ui.tinymce',
    ]);

})(angular);