(function() {
    'use strict';

    var module = angular.module('cards', [
        'ui.router',
        'oc.lazyLoad',
    ]);

    module.config(function($stateProvider) {
        $stateProvider
        .state('cards', {
            url: '/cards',
            component: 'cardsList',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                      // Cards common dependencies
                      '/static/js/app/cards/cards.services.js',
                      '/static/js/app/cards/card/card.module.js',
                      '/static/js/app/cards/card/card.component.js',

                      '/static/js/app/cards/cards-list/cards-list.module.js',
                      '/static/js/app/cards/cards-list/cards-list.component.js',
                      '/static/js/app/accounts/users.services.js',
                  ],
                  {
                    serie: true,
                  }
                );
              }]
            },
            // deepStateRedirect: true,
            // sticky: true,
        })
        .state({
            name: 'cards.card',
            url: '/:cardId',
            component: 'cardDetail',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                      '/static/js/app/cards/card-detail/card-detail.module.js',
                      '/static/js/app/cards/card-detail/card-detail.component.js',

                      // Cards common dependencies
                      '/static/js/app/cards/cards.services.js',
                      '/static/js/app/accounts/users.services.js',
                  ],
                  {
                    serie: true,
                  }
                );
              }]
            },
        })
        .state({
            name: 'cards.card.edit',
            url: '/edit',
            // component: 'cardEdit',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                      '/static/js/app/cards/card-edit/card-edit.module.js',
                      '/static/js/app/cards/card-edit/card-edit.component.js',
                      '/static/vendor/angular-dialogs/angular-dialogs.min.js',

                      // Cards common dependencies
                      // '/static/js/app/cards/cards.services.js',
                      '/static/js/app/accounts/users.services.js',
                      'static/js/app/i18n/edit-language-switcher/edit-language-switcher.module.js',
                      'static/js/app/i18n/edit-language-switcher/edit-language-switcher.services.js',
                      'static/js/app/i18n/edit-language-switcher/edit-language-switcher.component.js',
                  ],
                  {
                    serie: true,
                  }
                );
              }],
            },
            views: {
              // Relatively target the grand-parent-state's $default (unnamed) ui-view
              // This could also have been written using ui-view@state addressing: $default@messages
              // Or, this could also have been written using absolute ui-view addressing: !$default.$default.$default
              '^.^.$default': {
                component: 'cardEdit'
              },
            },
        })
        .state({
            name: 'cards.new',
            url: '/new',
            component: 'cardEdit',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                      '/static/js/app/cards/card-edit/card-edit.module.js',
                      '/static/js/app/cards/card-edit/card-edit.component.js',
                      '/static/vendor/angular-dialogs/angular-dialogs.min.js',

                      // Cards common dependencies
                      '/static/js/app/cards/cards.services.js',
                      '/static/js/app/accounts/users.services.js',
                      'static/js/app/i18n/edit-language-switcher/edit-language-switcher.module.js',
                      'static/js/app/i18n/edit-language-switcher/edit-language-switcher.services.js',
                      'static/js/app/i18n/edit-language-switcher/edit-language-switcher.component.js',
                  ],
                  {
                    serie: true,
                  }
                );
              }]
            },
        })

    //   $stateProvider
    //   .state('themeEdit', {
    //     url: '/theme/{themeId}',
    //     templateUrl: '/theme-edit.template.html',
    //     controller: 'ThemeEditCtrl',
    //   })
    //   .state('themeList', {
    //     url: '/themes/',
    //     templateUrl: '/theme-list.template.html',
    //     controller: 'ThemeListCtrl',
    //   })

    //   $stateProvider
    //   .state('formatEdit', {
    //     url: '/format/{formatId}',
    //     templateUrl: '/format-edit.template.html',
    //     controller: 'FormatEditCtrl',
    //   })
    //   .state('formatList', {
    //     url: '/formats/',
    //     templateUrl: '/format-list.template.html',
    //     controller: 'FormatListCtrl',
    //   })
    });
})();
