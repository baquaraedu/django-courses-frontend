class CardEditCtrl {
    constructor(
        $scope,
        $state,
        $stateParams,
        $sce,
        Cards,
        Likes,
        Audiences,
        Axes,
        Tags,
        CardFile,
        Images,
        YouTubeEmbeds,
        UserActions,
        gettextCatalog,
        EditLanguageSwitcherService,
        CardsHelpers,
        $dialogs
    ) {
        var ctrl = this;
        this.$scope = $scope;
        this.$state = $state;
        this.$dialogs = $dialogs;
        this.gettextCatalog = gettextCatalog;

        ctrl.last_slide_index = -1;
        $scope.error_messages = [];

        /* Slider */
        $scope.slides = [];
        $scope.mode = {
            ADD_MEDIA: 0,
            ADD_IMAGE: 1,
            ADD_VIDEO: 2,
            SHOW_MEDIA: 3,
        };
        $scope.slide_mode = $scope.mode.ADD_MEDIA;
        $scope.dirty_slide = true;
        $scope.active_slide = 0;

        // TODO remover esta logica, pq sempre vai ser editing mode, isso eh legado
        $scope.editing_mode = false;

        $scope.proxy = {};
        $scope.proxy.tags = [];

        let card_id = $stateParams.cardId;
        if (card_id) {
            $scope.card = Cards.get({ id: card_id }, function (card) {
                // $scope.card = card;
                $scope.originalCard = angular.copy(card);
                const image_slides = ($scope.card.image_gallery || []).map(function (img) {
                    return { type: 'image', data: img, card_slide_index: img.card_slide_index };
                });
                const video_slides = ($scope.card.youtube_embeds || []).map(function (video) {
                    return { type: 'video', data: video, card_slide_index: video.card_slide_index };
                });
                $scope.slides = [...image_slides, ...video_slides].sort(CardsHelpers.sort_slides_by_index);

                if ($scope.slides.length > 0) {
                    $scope.slide_mode = $scope.mode.SHOW_MEDIA;
                    $scope.dirty_slide = false;
                    $scope.selected_slide = $scope.slides[0];
                    $scope.selected_slide_index = 0;
                    ctrl.last_slide_index = $scope.slides[$scope.slides.length - 1].card_slide_index || -1;
                }
                else {
                    $scope.slide_mode = $scope.mode.ADD_MEDIA;
                    $scope.dirty_slide = true;
                }
            });

            // TODO check this
            $scope.editing_mode = true;

        } else {
            // New card
            $scope.card = {
                is_certified: false,
                audience: {},
                axis: {},
                image_gallery: [],
                youtube_embeds: [],
                authors: [],
            };
            this.$scope.originalCard = angular.copy($scope.card);
        }

        this.card_language = 'en';
        this.card_translations = ['hint', 'text', 'title', 'you_will_need'];

        $scope.audiences = Audiences.query();
        $scope.axes = Axes.query();
        $scope.tags = Tags.query();

        // (new UserActions({
        //     verb: "access",
        //     action_object_id: $stateParams.cardId,
        //     action_object_type: "Card",
        //     target_id: $stateParams.classroomId,
        //     target_type: "Classroom"
        //   })).$save();

        $scope.new_tag = function (new_tag) {
            if (new_tag.length <= 12) {
                return {
                    name: new_tag.toLowerCase()
                };
            }
            else {
                window.alert(gettextCatalog.getString(
                    'It is not allowed to insert a tag with more than 12 characters.'));
            }
        };
        $scope.tag_exists = function (new_tag) {
            for (var i = 0; i < $scope.tags.length; i++)
                if ($scope.tags[i].name == new_tag)
                    return true;
            return false;
        };

        $scope.add_author = function () {
            $scope.card.authors.push({
                author_name: "",
                author_description: "",
            });
        };

        $scope.new_slide = function (index) {
            $scope.selected_slide_index = $scope.slides.length + 1;
            $scope.slide_mode = $scope.mode.ADD_MEDIA;
            $scope.dirty_slide = true;
        };

        function valid_card() {
            $scope.error_messages = [];
            if (!$scope.card.title || $scope.card.title == '')
                $scope.error_messages.push(gettextCatalog.getString('Title is required field!'));
            if (!$scope.card.audience || !$scope.card.audience.id || $scope.card.audience.id == '')
                $scope.error_messages.push(gettextCatalog.getString('Theme is required field!'));
            if (!$scope.card.axis || !$scope.card.axis.id || $scope.card.axis.id == '')
                $scope.error_messages.push(gettextCatalog.getString('Format is required field!'));
            return $scope.error_messages.length == 0;
        }

        this.save = function() {

            if (valid_card()) {
                // $scope.card.id = $scope.card_id;
                $scope.card.image_gallery = $scope.slides.filter(function(media) {
                    return media.type == 'image';
                }).map(function(media) {
                    return media.data;
                });
                $scope.card.youtube_embeds = $scope.slides.filter(function(media) {
                    return media.type == 'video';
                }).map(function(media) {
                    return media.data;
                });
                $scope.card.tags = $scope.proxy.tags.map(function(tag) {
                    return tag.name;
                });
                if ($scope.card.id) {
                    EditLanguageSwitcherService.saveLocale($scope.card, this.card_translations, this.card_language);
                    Cards.update($scope.card, card => {
                        this.canExit = true;
                    },
                    function(error) {
                        $scope.error_messages.push(gettextCatalog.getString('Unable to save the content.'));
                        console.error(error);
                    });
                } else {
                    if (!('id' in $scope.card.axis)) {
                        $scope.card.axis = {};
                    }
                    if (!('id' in $scope.card.audience)) {
                        $scope.card.audience = {};
                    }

                    $scope.card = Cards.save($scope.card, card => {
                        this.canExit = true;
                    },
                    function (error) {
                        $scope.error_messages.push(
                            gettextCatalog.getString("Unable to create content.")
                        );
                        console.error(error);
                    })
                }
            }
        }

        /* Images */
        $scope.upload_image = function (file) {
            if (file) {
                const card_slide_index = ++ctrl.last_slide_index;
                Images.upload(file, { card_slide_index })
                    .then(function (response) {
                        $scope.slides.push({
                            type: "image",
                            data: response.data,
                            card_slide_index,
                        });
                        $scope.selected_slide_index = $scope.slides.length - 1;
                        $scope.selected_slide = $scope.slides[$scope.selected_slide_index];
                        $scope.dirty_slide = false;
                        $scope.slide_mode = $scope.mode.SHOW_MEDIA;
                    })
                    .catch(function (error) {
                        $scope.error_messages.push(
                            gettextCatalog.getString("Unable to save image.")
                        );
                        console.error(error);
                    });
            }
        };
        function remove_image(index) {
            if (window.confirm(
                gettextCatalog.getString("Do you really want to delete this image?"))) {
                return Images.delete({ id: $scope.slides[index].data.id }).$promise;
            }
        }

        /* Files */
        $scope.uploadCardFiles = function (file, card) {
            if (file) {
                CardFile.upload(file).then(
                    function (response) {
                        var card_file = new CardFile(response.data);

                        if (card.files === undefined)
                            card.files = [];
                        card.files.push(card_file);
                        return { location: card_file.file };
                    },
                    function (response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ": " + response.data;
                        }
                    },
                    function (evt) {
                        card.progress = Math.min(
                            100,
                            parseInt((100.0 * evt.loaded) / evt.total)
                        );
                    }
                );
            }
        };

        /* Videos */
        $scope.embed_video = function () {
            var youtube_pattern = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var result = youtube_pattern.exec($scope.video_url);
            $scope.video_url = "";
            var youtube_id;
            if (result && result[2].length == 11) {
                youtube_id = result[2];
            }
            const card_slide_index = ++ctrl.last_slide_index;
            YouTubeEmbeds.save({ card_slide_index, video_id: youtube_id })
                .$promise.then(function (response) {
                    $scope.slides.push({
                        type: "video",
                        data: response,
                        card_slide_index,
                    });
                    $scope.selected_slide_index = $scope.slides.length - 1;
                    $scope.selected_slide = $scope.slides[$scope.selected_slide_index];
                    $scope.slide_mode = $scope.mode.SHOW_MEDIA;
                    $scope.dirty_slide = false;
                    $scope.video_url = "";
                })
                .catch(function (error) {
                    $scope.error_messages.push(
                        gettextCatalog.getString(
                            "Could not save video."
                        )
                    );
                    console.error(error);
                });
        };
        function remove_video(index) {
            if (window.confirm(
                gettextCatalog.getString("Do you really want to delete this video?")))
                return YouTubeEmbeds.delete({
                    id: $scope.slides[index].data.id,
                }).$promise;
        }

        /* Media - Generic */
        $scope.select_media = function (index) {
            $scope.selected_slide_index = index;
            $scope.selected_slide = $scope.slides[index];
            $scope.slide_mode = $scope.mode.SHOW_MEDIA;
        };

        $scope.remove_media = function (index) {
            var media_type, promise;
            if ($scope.slides[index].type == "image") {
                media_type = "image";
                promise = remove_image(index);
            } else if ($scope.slides[index].type == "video") {
                media_type = "video";
                promise = remove_video(index);
            }
            promise.then(
                function () {
                    /* Media was successfully removed. */
                    $scope.slides.splice(index, 1);
                    if ($scope.slides.length == index) {
                        /* We removed the last-index slide */
                        if (index == 0) {
                            /* No more slides remaining */
                            $scope.slide_mode = $scope.mode.ADD_MEDIA;
                            $scope.dirty_slide = true;
                        } else {
                            $scope.selected_slide_index = $scope.slides.length - 1;
                            $scope.selected_slide =
                                $scope.slides[$scope.selected_slide_index];
                        }
                    } else {
                        $scope.selected_slide_index = index;
                        $scope.selected_slide = $scope.slides[index];
                    }
                },
                function (error) {
                    /* Media removal failed. */
                    if (media_type == "image")
                        $scope.error_messages.push(
                            gettextCatalog.getString(
                                "Could not delete image.")
                        );
                    else if (media_type == "video")
                        $scope.error_messages.push(
                            gettextCatalog.getString(
                                "Unable to delete the video.")
                        );
                    console.error(error);
                }
            );
        };

        $scope.is_selected_media = function (index) {
            if (($scope.selected_slide_index == index) ||
                ($scope.mode.ADD_MEDIA && $scope.slides.length == index - 1)) {
                return "btn-primary";
            }
            return "btn-default";
        };

        $scope.safe_url = function (url) {
            return $sce.trustAsResourceUrl(url);
        };

        $scope.safe_html = function (text) {
            return $sce.trustAsHtml(text);
        };
    }

    uiCanExit() {
        if (this.canExit || angular.equals(this.$scope.card, this.$scope.originalCard)) {
          return true;
        }
        this.$dialogs.showConfirmationDialog(
            this.gettextCatalog.getString("If you leave, there are unsaved changes that will be lost!"),
            {
                title: this.gettextCatalog.getString("Leave whithout saving and discard changes?"),
                closable: true,
                callback: (option) => {
                    if(option === "ok") {
                        this.canExit = true;
                        this.$state.go("^", null, { reload: true });
                    }
                }
            }
        );
        return false;
    }

    delete() {
        this.$dialogs.showConfirmationDialog(
            this.gettextCatalog.getString("If you delete this content it cannot be recovered!"
        ), {
            title: this.gettextCatalog.getString("Delete this content?"),
            closable: true,
            buttonOkText: "Delete",
            callback: (option) => {
                if(option === "ok") {
                    this.$scope.card.$delete().
                        catch(function(error) {
                            this.$scope.error_messages.push(
                                this.gettextCatalog.getString('Unable to remove the practice.'
                            ));
                            console.error(error);
                        });;
                    this.canExit = true;
                    this.$state.go("^.^", null, { reload: true });
                }
            }
        });
    };

    cancel() {
        this.$state.go("^", null, { reload: true });
    };
}

CardEditCtrl.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    '$sce',
    'Cards',
    'Likes',
    'Audiences',
    'Axes',
    'Tags',
    'CardFile',
    'Images',
    'YouTubeEmbeds',
    'UserActions',
    'gettextCatalog',
    'EditLanguageSwitcherService',
    'CardsHelpers',
    '$dialogs',
];

angular.
module('cardEdit').
    component('cardEdit', {
        templateUrl: '/card-edit.template.html',
        controller: CardEditCtrl,
        bindings: {
            card: '<',
        },
});
