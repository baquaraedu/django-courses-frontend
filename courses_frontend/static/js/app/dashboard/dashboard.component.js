(function(angular){
  'use strict';
  var app = angular.module('dashboard');

  angular.
  module('dashboard').
  component('dashboard', {
    templateUrl: '/dashboard.template.html',
    controller: 'DashboardCtrl',
  });

  app.controller('DashboardCtrl', [
      '$scope',
      'Course',
      'CourseStudent',
      'MyCourses',
      'Topic',
      'ForumPage',
      'UserAccess',
      '$window',
      // 'Cards',
      function (
          $scope,
          Course,
          CourseStudent,
          MyCourses,
          Topic,
          ForumPage,
          UserAccess,
          $window,
          // Cards
      ) {

          $scope.forums = [];
          // Log this access
          (new UserAccess({ area: 'dashboard' })).$save().then(
              (success) => {},
              (error) => {
                  console.warn("The area: 'dashboard' was not registered in UserAccess")
              }
          )

          $scope.getMessage = function(id) {
              $window.location.href = "/#!/messages/"+id;
          }

          function compare_by_position(a,b) {
              if (a.home_position !== null && b.home_position !== null) {
                  if (a.home_position < b.home_position)
                     return -1;
                  if (a.home_position > b.home_position)
                     return 1;
              } else if (a.home_position !== null && b.home_position === null)
                  return -1;
              else if (a.home_position === null && b.home_position !== null)
                  return 1;
              return 0;
          }

          function compare_by_course_student(a,b) {
              if (a.course_student === undefined) {
                  return 1;
              } else  {
                  if (b.course_student !== undefined) {
                      if (b.course_student.last_activity < a.course_student.last_activity)
                          return -1;
                      else
                          return 1;
                  } else {
                      return -1;
                  }
              }
              return 0;
          }

          function fetchDashboardData() {
              MyCourses.query({}, function (my_courses) {
                  $scope.my_courses = my_courses;

                  CourseStudent.query({}, function (course_students){
                      $scope.course_students = course_students
                      angular.forEach($scope.my_courses, function(my_course) {
                          $scope.langs = []
                          angular.forEach(my_course.lang, function(lang) {
                              let langs_dict = {
                                  'en': 'English',
                                  'es': 'Spanish',
                                  'pt-br': 'Portuguese'
                              }
                              $scope.langs.push(langs_dict[lang]);
                          });
                          my_course.lang = $scope.langs
                          angular.forEach($scope.course_students, function(course_student) {
                              if (my_course.id === course_student.course.id) {
                                  my_course.course_student = course_student;
                              }
                          });
                      });
                  });
              });

              try{
                  ForumPage.get({
                      page: 1,
                      page_size: 10,
                  }, (response) => {
                      $scope.forums = response.results
                  });
              }
              catch(err){
                  console.error(err)
              }

              Course.query({}, function (courses) {
                  courses.sort(compare_by_position);
                  $scope.courses = courses;
                  CourseStudent.query({}, function (course_students){
                      $scope.course_students = course_students
                      angular.forEach($scope.courses, function(course) {
                          $scope.langs = []
                          angular.forEach(course.lang, function(lang) {
                              let langs_dict = {
                                  'en': 'English',
                                  'es': 'Spanish',
                                  'pt-br': 'Portuguese'
                              }
                              $scope.langs.push(langs_dict[lang]);
                          });
                          course.lang = $scope.langs
                          angular.forEach($scope.course_students, function(course_student) {
                              if (course.id === course_student.course.id) {
                                  course.course_student = course_student;
                              }
                          });
                      });
                  });

                  let coursesByTrack = {}
                  angular.forEach(courses, function(course) {
                      let trackName = course.track.name;
                      if (trackName === undefined) {
                          trackName = 'no track';
                      }

                      if (!(trackName in coursesByTrack)) {
                          coursesByTrack[trackName] = {'courses': [], 'home_position': null};
                      }

                      coursesByTrack[trackName]['courses'].push(course);
                      let position = (course.track.home_position !== undefined ? course.track.home_position : null);
                      coursesByTrack[trackName]['home_position'] = position;
                  });

                  $scope.coursesByTrack = Object.keys(coursesByTrack).map(function(key) {
                      return {'name': key, 'courses': coursesByTrack[key].courses,
                              'home_position': coursesByTrack[key].home_position };
                  });

                  $scope.coursesByTrack.sort(compare_by_position);
              });

              // Cards.get({
              //         is_certified: 2,
              //         limit: 3
              //     }, function(data){
              //         $scope.cards = data.results;
              //     });

              // $scope.card_image = function(card) {
              //     if (card.image_gallery.length > 0)
              //         return card.image_gallery[0].image;
              //     return '/static/img/card-default.png';
              // };
          };

          fetchDashboardData();
          $scope.$on('languageChanged', function() {
              fetchDashboardData();
          });
      }
  ]);

  app.controller('MessagesDashboardController', [
      '$scope',
      '$sce',
      'MessageBasic',
      function(
          $scope,
          $sce,
          MessageBasic,
      ) {
          function fetchMessages () {
              $scope.messages = MessageBasic.query({my_messages: true, unread: true, limit_to: 5});
              $scope.messages_unread_count = MessageBasic.unread_count();
          }

          fetchMessages();
          $scope.$on('languageChanged', fetchMessages);
          $scope.$on('announcementRead', fetchMessages);

          $scope.strip_html = function(html_content) {
              var text = html_content ? "<p>"+String(html_content).replace(/<[^>]+>/gm, '')+"</p>" : '';
              return $sce.trustAsHtml(text);
          };
      }
  ]);

})(window.angular);
