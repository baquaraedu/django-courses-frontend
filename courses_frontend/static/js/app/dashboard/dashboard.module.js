(function(angular){
    'use strict';

    angular.module('dashboard', [
        'django',
        'core.services',
        'discussion.services',
        'messages.services',
        'ngFileUpload',
        'ngRoute',
        'ngSanitize',
        'ui.bootstrap',
        'ui.select',
        'ui.tinymce',
        'ui.router', 
        //'shared',
    ]);
})(angular);
