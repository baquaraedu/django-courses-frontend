(function(angular){
    'use strict';

    var app = angular.module('baquara.services', []);

    app.factory('UserLocalStorage', function(store) {
        return store.getNamespacedStore('local-user-storage');
    });

    app.factory('SetLanguage', ['$http', function($http){
        return function(language){
            return $http({
                method:'POST',
                url: API_URL + '/i18n/setlang/',
                data: 'language=' + language,
                headers:{'Content-Type':'application/x-www-form-urlencoded'}
            });
        };
    }]);

})(angular);
