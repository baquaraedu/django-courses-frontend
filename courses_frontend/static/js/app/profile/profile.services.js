(function (angular) {
    'use strict';
    var module = angular.module('profile.services', ['ngResource']);

    module.factory('UserProfile', function($resource){
        return $resource(BASE_API_URL + '/profile/:userId', {}, {
        });
    });

    module.factory('City', function($resource){
        return $resource(API_URL + '/address/cities/:id', {}, {
        });
    });


    module.factory('CityAutocomplete', ['$http', function($http){
        return function(val) {
            return $http.get(API_URL + '/address/autocomplete/city/', {
                params: {
                    q: val,
                }
            }).then(function (res) {
                return res.data.results;    
            });
        };
    }]);

})(angular);