(function(angular){
    'use strict';

    angular.module('profile', [
        'profile.controllers',
        'profile.services',
        'core.services',
        'courses.services',
        'djangular',
        'ui.bootstrap',
        'ui.tinymce',
        'ngFileUpload',
        'shared',
    ]);
})(angular);
