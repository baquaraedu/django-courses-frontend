(function(angular){
    'use strict';
    var app = angular.module('profile.controllers', ['ngSanitize']);

    app.controller('UserProfileController', [
        '$scope',
        '$location',
        'UserProfile',
        'CurrentUser',
        'UserAccess',
        'MyCourses',
        'CourseStudent',
        'CityAutocomplete',
        'City',
        'gettextCatalog',

        function ($scope, $location, TimtecUser, CurrentUser, UserAccess, MyCourses, CourseStudent, CityAutocomplete, City, gettextCatalog) {
            
            $scope.language_options = [
                {id:'', name:''},
                {id:'pt-br', name:'PT-BR'},
                {id:'en', name:'EN'},
                {id:'es', name:'ES'}
            ];

            // Log this access
            (new UserAccess({ area: 'profile-page' })).$save().then(
                (success) => {},
                (error) => {
                    console.warn("The area: 'profile-page' was not registered in UserAccess")
                }
            );

            // Set buttons for the tinyMCE editor bio field
            $scope.tinymceOptions = {
                toolbar: 'bold italic | bullist numlist outdent indent | quicklink link | removeformat'
              };

            $scope.current_user = CurrentUser;
            
            $scope.save_profile = function(username) {
                return confirm(`${gettextCatalog.getString('You confirm the change to the user:')} ${username}?`);
            }
            
            // remove the last slash
            var abs_url = $location.absUrl();
            if (abs_url.slice(-1) === '/') {
                abs_url = abs_url.slice(0, -1);
            }
            
            var username = abs_url.split('/').pop();
            
            if (username === 'profile' || username === 'edit') {
                username = CurrentUser.username;
            }

            $scope.cityName = '';
                           
            $scope.getCities = function(val) {
                return new CityAutocomplete(val)
            };
            
            $scope.on_select_city = function(val) {
                if ($scope.user_profile == null ){
                    $scope.user_profile = {                     
                    }
                }
                $scope.user_profile.city = val.id;
            };

            TimtecUser.get({username: username}, function(user_profile) {

                if (user_profile.city){
                    City.get({id: user_profile.city}, function(city) {
                        $scope.cityName = city.display_name;
                    });
                }
                else{
                    $scope.cityName ='';
                }
                user_profile.is_current_user = (user_profile.id === parseInt(CurrentUser.id, 10));
                $scope.user_profile = user_profile;
                
                // Try to find at least one valid certificate
                $scope.valid_certificates = false;
                for (var i = 0; i < user_profile.certificates.length; i++) {
                    if(user_profile.certificates[i].approved === true){
                        $scope.valid_certificates = true;
                        break;
                    }
                }

                if (user_profile.is_current_user) {
                    MyCourses.query({}, function (my_courses) {
                        $scope.my_courses = my_courses;

                        CourseStudent.query({}, function (course_students){
                            $scope.course_students = course_students;

                            angular.forEach($scope.my_courses, function(my_course) {
                                $scope.langs = [];
                                angular.forEach(my_course.lang, function(lang) {
                                    let langs_dict = {
                                        'en': 'English',
                                        'es': 'Spanish',
                                        'pt-br': 'Portuguese'
                                    }
                                    $scope.langs.push(langs_dict[lang]);
                                });
                                my_course.lang = $scope.langs;
                                angular.forEach($scope.course_students, function(course_student) {
                                    if (my_course.id === course_student.course.id) {
                                        my_course.course_student = course_student;
                                    }
                                });
                            });
                        });
                    });
                }
            }, function(error) {
                $scope.user_profile = null;
            });
        }
    ]);

    app.directive("file", function () {
      return {
        require: "ngModel",
        restrict: "A",
        link: function ($scope, el, attrs, ngModel) {
          el.bind("change", function (event) {
            const imgtag = document.getElementById("profile-avatar");
            const img = document.createElement("img");
            const selectedFile = event.target.files[0];
            const reader = new FileReader();
            reader.onload = function(event) {
                img.src = event.target.result;
                const canvas = document.createElement("canvas");
                let ctx = canvas.getContext("2d");

                img.onload = () => {
                    ctx.drawImage(img, 0, 0);
                    const MAX_WIDTH = 400;
                    const MAX_HEIGHT = 300;
                    let width = img.width;
                    let height = img.height;

                    if (width > height) {
                        if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width;
                            width = MAX_WIDTH;
                        }
                    } else {
                        if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0, width, height);

                    const dataurl = canvas.toDataURL("image/png");
                    imgtag.style.backgroundImage = `url(${dataurl})`;
                }
            };
            reader.readAsDataURL(selectedFile);
            ngModel.$setViewValue(selectedFile);
            $scope.$apply();
          });
        },
      };
    });
})(window.angular);
