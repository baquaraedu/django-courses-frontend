(function() {
    'use strict';

    angular.
    module('profile').
    component('profile', {
      templateUrl: '/profile.template.html',
      controller: 'UserProfileController',
    });
})();
