(function(angular){
    'use strict';
    var app = angular.module('mainNav.controllers', []);

    app.controller('MainNavCtrl', [
        '$scope', 'CurrentUser', '$rootScope', '$window', '$sce', 'UserProfile', 'MessageBasic',
        function ($scope, CurrentUser, $rootScope, $window, $sce, UserProfile, Message) {
            var ctrl = this;
            ctrl.dashboardLocation = ''
            ctrl.coursesLocation = ''

            $scope.current_origin = $window.location.origin;

            ctrl.trustUrl = function(url){
              return $sce.getTrustedResourceUrl(url);
            };

            $scope.getLocation = function(newUrl){
                return $sce.getTrustedResourceUrl($window.location.host + newUrl);
            };

            if (CurrentUser.username) {
                $scope.user = CurrentUser;
            }
            else {
                UserProfile.get({id: CurrentUser.id}, function(current_user) {
                    $scope.user = current_user;
                }, function(error) {
                    $scope.user = null;
                });
            }

            $scope.toggle_main_nav_display = function() {
                $rootScope.is_main_nav_opened = !$rootScope.is_main_nav_opened;
                $scope.is_main_nav_opened = $rootScope.is_main_nav_opened;
            };

            // TODO: Eventually remove jQuery dependency
            $('.main-nav a[href]').click(() => {
                if ($rootScope.is_main_nav_opened) {
                    $rootScope.is_main_nav_opened = false;
                    $scope.is_main_nav_opened = false;
                }
            });

            $scope.setLocations = function(){
                ctrl.dashboardLocation = $window.location.host + '#!/dashboard';
                ctrl.coursesLocation = $window.location.host + '#!/courses';
            }

            function fetchMessages() {
                $scope.messages_unread_count = Message.unread_count();
            }

            fetchMessages();
            $scope.$on('announcementRead', fetchMessages);

            // Chat integration related code beyond here
            // $scope.chat = {
            //     notifications: 0
            // };
            // Listens to 'message' events triggered by the Rocket Chat iframe
            // window.addEventListener('message', function(e) {
            //     if(e.data.eventName === 'unread-changed'){
            //         const counter = e.data.data;
            //         if(counter > 9)
            //             // if there are more than 9 notifications, reduce the number
            //             $scope.chat.notifications = "9+";
            //         else if(counter === "" || counter === "•")
            //             // If the data value is empty, then it must be set to zero
            //             $scope.chat.notifications = "0";
            //         else
            //             // Otherwise, the number must be used as is
            //             $scope.chat.notifications = counter;

            //         // Ensures that the template is updated as soon as possible
            //         $scope.$apply();
            //     }
            // });
        }
    ]);

})(window.angular);
