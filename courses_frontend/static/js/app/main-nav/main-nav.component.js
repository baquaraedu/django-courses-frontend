(function() {
    'use strict';

    angular.
    module('mainNav').
    component('mainNav', {
      templateUrl: '/main-nav.template.html',
    });
})();
