(function() {
    'use strict';

    angular.module('mainNav', [
        'ui.router',
        'mainNav.controllers',
        'mainNav.services',
        'messages.services',
    ]);

})();
