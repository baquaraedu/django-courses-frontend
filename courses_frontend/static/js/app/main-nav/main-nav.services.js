(function (angular) {
    'use strict';

    var module = angular.module('mainNav.services', ['ngRoute', 'ngResource']);

    module.factory('UserProfile', function($resource){
        return $resource(BASE_API_URL + '/profile/:userId', {}, {
        });
    });

})(angular);
