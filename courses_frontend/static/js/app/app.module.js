(function(angular) {
    'use strict';

    var app = angular.module('baquara', [
        'ui.router',
        'oc.lazyLoad',
        'gettext',
        'i18n',
        'mainHeader',
        'mainHeaderUnlogged',
        'mainNav',
        'courses',
        'baquara.controllers',
        'baquara.services',
        'forum',
        'topic',
        'new-topic',
        'categoryEdit',
        'categoryList',
        'themeEdit',
        'themeList',
        'formatEdit',
        'formatList',
        'angular-storage',
        'courseCard',
        'cardsCarousel',
        'course',
        'courses.services',
        'courses.filters',
        'profile',
        'shared',
        'filters.htmlentities',
        'generalReports',
        'userReports',
        'userReport',
        'userAccessReport',
        'userActionsReport',
        'userCourseReportList',
        'userCourseReport',
        'signup.controllers',
        'authorization.services',
    ]);

    app.config(function($stateProvider, $urlServiceProvider) {

      $urlServiceProvider.rules.otherwise({ state: 'dashboard' });
      // This is the new pattern that must be followed, with lazyloading modules
      // This code just lazy load the nested modules, all chaild routes must be defined inside the module
      $stateProvider
      .state({
        name: 'baquara',
        url: '',
      })

      .state({
        name: 'events.**',
        url: '/events',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load(
            ['/static/js/app/events/events.module.js']
          );
        },
      })
      .state({
        name: 'messages.**',
        url: '/messages',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load(
            ['/static/js/app/messages/messages.module.js']
          );
        },
      })
      .state({
        name: 'cards.**',
        url: '/cards',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load(
            ['/static/js/app/cards/cards.module.js']
          );
        },
      })
      .state({
        name: 'home',
        url: '/home',
        component: 'home',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load(
            [
              '/static/js/app/home/home.module.js',
              '/static/js/app/home/home.component.js'
            ],
            { serie: true },
          )
        }
      })

      // -------------- End new good pattern, below here are legacy pattern and pratices that must not be reproduced ---------

        var dashboard = {
          name: 'dashboard',
          url: '/dashboard',
          component: 'dashboard',
          lazyLoad: function ($transition$) {
            return $transition$.injector().get('$ocLazyLoad').load(
              [
                'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
                'https://unpkg.com/moment-timezone@0.5.34/builds/moment-timezone-with-data-10-year-range.min.js',
                'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.js',
                'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.css',

                '/static/js/app/accounts/accounts.services.js',
                '/static/js/app/events/events.module.js',
                '/static/js/app/events/events.services.js',
                '/static/js/app/events/events-list/events-list.module.js',
                '/static/js/app/events/events-list/events-list.component.js',
                // '/static/js/app/cards/he.filters.js',

                '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.module.js',
                '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.services.js',
                '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.component.js',

                '/static/js/app/dashboard/dashboard.module.js',
                '/static/js/app/dashboard/dashboard.component.js',
              ],
              { serie: true },
            );
          },
        }

        var courses = {
          name: 'courses',
          url: '/courses',
          component: 'courses'
        }

        var profile = {
          name: 'profile',
          url: '/profile/{userId}',
          component: 'profile'
        }

        var generalReports = {
          name: 'general-reports',
          url: '/reports/general',
          component: 'generalReports',
          lazyLoad: function ($transition$) {
            return $transition$.injector().get('$ocLazyLoad').load([
                '/static/js/app/courses/courses.services.js',
                '/static/js/app/accounts/accounts.services.js',
            ]);
          },
        }

        var userReports = {
          name: 'user-reports',
          url: '/reports/users',
          component: 'userReports'
        }

        $stateProvider.state(dashboard);
        $stateProvider.state(courses);
        $stateProvider.state(profile);
        $stateProvider.state(generalReports);
        $stateProvider.state(userReports);

        $stateProvider
        .state('forum', {
            url: '/forum',
            templateUrl: '/forum.template.html',
            controller: 'ForumCtrl',
            lazyLoad: function ($transition$) {
              return $transition$.injector().get('$ocLazyLoad').load([
                '/static/vendor/angular-dialogs/angular-dialogs.min.js',
              ])
            },
        })
        .state('forumDetail', {
          url: '/forum/{forumId}',
          templateUrl: '/forum.template.html',
          controller: 'ForumCtrl',
          lazyLoad: function ($transition$) {
            return $transition$.injector().get('$ocLazyLoad').load(
              [
                'https://unpkg.com/angular-scroll@1.0.2/angular-scroll.min.js',
                '/static/vendor/angular-dialogs/angular-dialogs.min.js',

                '/static/js/app/forum/forum/forum.module.js',
                '/static/js/app/forum/forum/forum.controllers.js',
                '/static/js/app/forum/forum/forum.services.js',
                '/static/js/app/forum/forum/forum.directives.js',
                '/static/js/app/forum/forum/forum.filters.js',
              ],
              { serie: true },
            );
          },
        })

        $stateProvider
        .state('topic', {
          url: '/topic/{topicId}',
          templateUrl: '/topic.template.html',
          controller: 'TopicCtrl',
          lazyLoad: function ($transition$) {
            return $transition$.injector().get('$ocLazyLoad').load(
              [
                'https://unpkg.com/angular-scroll@1.0.2/angular-scroll.min.js',
                '/static/vendor/angular-dialogs/angular-dialogs.min.js',

                '/static/js/app/forum/topic/topic.module.js',
                '/static/js/app/forum/topic/topic.controllers.js',
                '/static/js/app/forum/forum/forum.services.js',
                '/static/js/app/forum/forum/forum.directives.js',
                '/static/js/app/forum/forum/forum.filters.js',
              ],
              { serie: true },
            );
          },
        })
        .state('newTopic', {
          url: '/topic/new/',
          templateUrl: '/new-topic.template.html',
          controller: 'NewTopicCtrl',
        })

        $stateProvider
        .state('categoryEdit', {
          url: '/category/{categoryId}',
          templateUrl: '/category-edit.template.html',
          controller: 'CategoryEditCtrl',
        })
        .state('categoryList', {
          url: '/categories/',
          templateUrl: '/category-list.template.html',
          controller: 'CategoryListCtrl',
        })

        $stateProvider
        .state('themeEdit', {
          url: '/theme/{themeId}',
          templateUrl: '/theme-edit.template.html',
          controller: 'ThemeEditCtrl',
        })
        .state('themeList', {
          url: '/themes/',
          templateUrl: '/theme-list.template.html',
          controller: 'ThemeListCtrl',
        })

        $stateProvider
        .state('formatEdit', {
          url: '/format/{formatId}',
          templateUrl: '/format-edit.template.html',
          controller: 'FormatEditCtrl',
        })
        .state('formatList', {
          url: '/formats/',
          templateUrl: '/format-list.template.html',
          controller: 'FormatListCtrl',
        })

        $stateProvider
        .state('course', {
          url: '/course/{courseSlug}',
          templateUrl: '/course.template.html',
          controller: 'CourseCtrl',
        })

    });

    angular.module('baquara').run([
      'AuthorizationService',
      'TranslationService',
      function (
        AuthorizationService,
        TranslationService,
      ) {
      }]);

    // Show state tree
    //app.run(function($uiRouter) {
    //  var StateTree = window['ui-router-visualizer'].StateTree;
    //  var el = StateTree.create($uiRouter, null, { height: 100, width: 300 });
    //  el.className = 'statevis';
    //});

})(angular);
