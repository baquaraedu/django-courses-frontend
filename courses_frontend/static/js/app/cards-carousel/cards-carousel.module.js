(function(angular){
    'use strict';

    var app = angular.module('cardsCarousel', [
        'cardsCarousel.controllers',
        'core.services',
        'django',
        'ngRoute',
        'ngResource',
        //'shared',
    ]);
})(angular);
