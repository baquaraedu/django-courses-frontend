(function(angular){
    'use strict';
    var app = angular.module('cardsCarousel.controllers', []);

    app.controller('cardsCarousel', [
        function () {
            var ctrl = this;
            this.$onInit = function () {
                
                var sliders = document.querySelectorAll('.glider')
                sliders.forEach(item => {
                    var prev = item.parentElement.querySelector('.glider-prev')
                    var next = item.parentElement.querySelector('.glider-next')
                    var configs = {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        itemWidth: 150,
                        draggable: true,
                        scrollLock: true,
                        rewind: true,
                        dots: '#dots',
                        arrows: {
                            prev: prev,
                            next: next
                        },
                        responsive: [
                            {
                                breakpoint: 800,
                                settings: {
                                    slidesToScroll: 'auto',
                                    itemWidth: 300,
                                    slidesToShow: 'auto',
                                    exactWidth: true
                                }
                            },
                            {
                                breakpoint: 700,
                                settings: {
                                    slidesToScroll: 4,
                                    slidesToShow: 4,
                                    dots: false,
                                    arrows: false,
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToScroll: 3,
                                    slidesToShow: 3
                                }
                            },
                            {
                                breakpoint: 500,
                                settings: {
                                    slidesToScroll: 2,
                                    slidesToShow: 2,
                                    dots: false,
                                    arrows: false,
                                    scrollLock: true
                                }
                            }
                        ]
                    }
                    new Glider(item, configs)
                })

            }
           
        }
    ]);
})(window.angular);
