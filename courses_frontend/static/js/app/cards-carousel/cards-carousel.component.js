(function() {
  'use strict';

  angular.
  module('cardsCarousel').
  component('cardsCarousel', {
    templateUrl: '/cards-carousel.template.html',
    controller: 'cardsCarousel',
    bindings: {
      courses: '<'
    },
  });
})();

