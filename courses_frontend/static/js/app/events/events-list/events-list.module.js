(function(angular) {
    'use strict';

    var module = angular.module('eventsList', [
        'events.services',
        'accounts.services',
        'editLanguageSwitcher',
        'moment-picker',
        'ui.bootstrap',
        'ui.tinymce',
    ]);

    // var app = angular.module( 'myApp', [] )
    module.config( [
        '$compileProvider',
        function($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|webcal):/);
        }
    ]);

})(angular);
