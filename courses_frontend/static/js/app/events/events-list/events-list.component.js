(function(angular) {
    'use strict';

    angular
        .module('eventsList')
        .component('eventsList', {
            templateUrl: '/events-list.template.html',
            transclude: {
                title: '?eventsListTitle'
            },
            controller: EventsListCtrl,
            bindings: {
                columns: '<',
                course: '<',
                limit: '<',
            },
        });

    EventsListCtrl.$inject = [
        '$scope',
        '$sce',
        '$location',
        '$uibModal',
        'Event',
        'User',
        'TranslationService',
    ];

    const tinymceOptions = {
        plugins: 'link',
        toolbar: 'undo redo | bold italic | link removeformat',
    };

    function EventsListCtrl (
        $scope,
        $sce,
        $location,
        $uibModal,
        Event,
        User,
        TranslationService
    ) {
        const ctrl = this;

        ctrl.displayedEvents = [];
        ctrl.events = [];
        ctrl.showAll = false;

        let locale = TranslationService.currentLanguage;
        if (!ctrl.course) {
            $scope.$on('languageChanged', function(event, data) {
                locale = data.newLanguage;
                ctrl.fetchEvents();
            });
        }

        const calculateShownEvents = () => {
            if (ctrl.limit && !ctrl.showAll) {
                ctrl.displayedEvents = ctrl.events.slice(0, ctrl.limit);
            } else {
                ctrl.displayedEvents = ctrl.events;
            }
        };

        ctrl.fetchEvents = function () {
            return Event.query({ course: ctrl.course }).$promise.then((events) => {
                ctrl.events = events;
                calculateShownEvents();
            });
        };

        ctrl.$onInit = function () {
            ctrl.fetchEvents();

            ctrl.icsUrl = 'https://' + $location.host() + '/api/calendar/calendar.ics'
            ctrl.webcalIcsUrl = 'webcal://' + $location.host() + '/api/calendar/calendar.ics'

            User.get_current((user) => {
                ctrl.user = user;

                let icsParams = ''
                icsParams += '?user=' + ctrl.user.id
                if (ctrl.course) {
                    icsParams += '&course=' + ctrl.course
                }
                ctrl.icsUrl += icsParams
                ctrl.webcalIcsUrl += icsParams
            });
        };

        ctrl.safeHtml = function (html) {
            return $sce.trustAsHtml(html);
        };

        ctrl.showAllEvents = function () {
            ctrl.showAll = true;
            ctrl.displayedEvents = ctrl.events;
        };

        ctrl.createEvent = function (course) {
            const CreateEventModalInstanceCtrl = ($scope, $uibModalInstance, EditLanguageSwitcherService) => {
                $scope.event = {
                    title: '',
                    text: '',
                    date_time: '',
                    type: course ? 'course' : 'general',
                    course: course || null,
                    classroom: null,
                };
                $scope.dateTime = {};
                $scope.dateTime.str = '';
                $scope.dateTime.obj = moment().locale(locale);

                $scope.locale = locale;
                $scope.translations = ['text', 'title'];
                $scope.tinymceOptions = tinymceOptions;

                $scope.cancelModal = function () {
                    $uibModalInstance.dismiss();
                };

                $scope.saveEvent = function () {
                    EditLanguageSwitcherService.saveLocale($scope.event, $scope.translations, $scope.locale);
                    $scope.event.date_time = $scope.dateTime.obj.format();
                    $uibModalInstance.close($scope.event);
                };
            };

            const modalInstance = $uibModal.open({
                templateUrl: 'create-event_modal.html',
                controller: [
                    '$scope',
                    '$uibModalInstance',
                    'EditLanguageSwitcherService',
                    CreateEventModalInstanceCtrl,
                ],
            });

            modalInstance.result.then((newEvent) => {
                Event.save(newEvent, function (createdEvent) {
                    ctrl.events = [...ctrl.events, createdEvent];
                    calculateShownEvents();
                });
            });
        };

        ctrl.editEvent = function (event) {
            const EditEventModalInstanceCtrl = ($scope, $uibModalInstance, EditLanguageSwitcherService) => {
                const eventBackup = Object.assign({}, event);

                $scope.event = event;
                $scope.dateTime = {};
                $scope.dateTime.str = '';
                $scope.dateTime.obj = moment(event.date_time).locale(locale);

                $scope.locale = locale;
                $scope.translations = ['text', 'title'];
                $scope.tinymceOptions = tinymceOptions;

                $scope.cancelModal = function () {
                    Object.assign(event, eventBackup);
                    $uibModalInstance.dismiss();
                };

                $scope.saveEvent = function () {
                    EditLanguageSwitcherService.saveLocale($scope.event, $scope.translations, $scope.locale);
                    $scope.event.date_time = $scope.dateTime.obj.format();
                    $uibModalInstance.close($scope.event);
                };
            };

            const modalInstance = $uibModal.open({
                templateUrl: 'create-event_modal.html',
                controller: [
                    '$scope',
                    '$uibModalInstance',
                    'EditLanguageSwitcherService',
                    EditEventModalInstanceCtrl,
                ],
            });

            modalInstance.result.then((editedEvent) => {
                Event.update(editedEvent, (updatedEvent) => {
                    Object.assign(event, updatedEvent);
                });
            });
        };

        ctrl.deleteEvent = function (event) {
            Event.delete(event, function () {
                ctrl.events = ctrl.events.filter(e => e.id != event.id);
                calculateShownEvents();
            });
        }
    }
})(angular);
