(function (angular) {
    'use strict';

    var app = angular.module('events.services', ['ngResource']);

    app.factory('Event', ['$resource', function ($resource) {
        return $resource('/api/events/:id', { id: '@id' },
            { update: { method: 'PUT' } },
        );
    }]);
})(angular);
