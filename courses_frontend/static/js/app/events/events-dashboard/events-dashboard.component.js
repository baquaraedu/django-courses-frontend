(function() {
    'use strict';

    angular.
    module('eventsDashboard').
    component('eventsDashboard', {
      templateUrl: '/events-dashboard.template.html',
    });
})();
