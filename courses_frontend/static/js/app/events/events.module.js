(function (angular) {
    'use strict';

    var app = angular.module('events', [
        'ui.router',
        'oc.lazyLoad',
    ]);

    app.config(function ($stateProvider) {
        $stateProvider
        .state('events', {
            url: '/events',
            component: 'eventsDashboard',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        [
                            'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
                            'https://unpkg.com/moment-timezone@0.5.34/builds/moment-timezone-with-data-10-year-range.min.js',
                            'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.js',
                            'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.css',

                            '/static/js/app/accounts/accounts.services.js',
                            '/static/js/app/events/events.services.js',
                            '/static/js/app/events/events-list/events-list.module.js',
                            '/static/js/app/events/events-list/events-list.component.js',
                            '/static/js/app/events/events-dashboard/events-dashboard.module.js',
                            '/static/js/app/events/events-dashboard/events-dashboard.component.js',
                            '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.module.js',
                            '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.services.js',
                            '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.component.js',
                        ],
                        { serie: true },
                    );
                }],
            },
        });
    });
})(angular);
