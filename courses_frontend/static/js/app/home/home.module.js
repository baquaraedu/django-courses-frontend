(function (angular) {
    'use strict';

    angular.module('home', [
        'courseCard',
        'courses.services',
        'ui.router',
    ])
})(angular);
