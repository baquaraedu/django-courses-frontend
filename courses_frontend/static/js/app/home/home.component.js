(function () {
    'use strict';

    class HomeCtrl {

        static get $inject () {
            return [
                '$scope',
                'Course',
                'TranslationService',
            ];
        }

        constructor ($scope, Course, TranslationService) {
            const ctrl = this;

            ctrl.courses = Course.query({ public_courses: 'True' });

            this.language = TranslationService.currentLanguage;
            $scope.$on('languageChanged', (event, data) => {
                this.language = data.newLanguage;
                ctrl.courses = Course.query({ public_courses: 'True' });
            });
        }

        get lang () {
            return this.language.replace('_', '-');
        }

        get logo () {
            switch (this.language) {
                case 'pt_br':
                    return '/static/img/logo-green-pt.png';
                case 'es':
                    return '/static/img/logo-green-es.png';
                default:
                    return '/static/img/logo-green-en.png';
            }
        }
    }

    angular.module('home').component('home', {
        templateUrl: '/home.template.html',
        controller: HomeCtrl,
    });
})();
