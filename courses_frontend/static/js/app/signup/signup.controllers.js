(function(angular){
    'use strict';
    var app = angular.module('signup.controllers', ['ngSanitize']);

    app.controller('UserSignupController', [
        '$scope',
        'CityAutocomplete',
        function (
            $scope,
            CityAutocomplete,
        ) {
            $scope.language_options = [
                {id:'pt-br', name:'pt-br'},
                {id:'en', name:'en'},
                {id:'es', name:'es'}
            ];

            $scope.cityName = '';
            $scope.getCities = function(val) {
                return new CityAutocomplete(val)
            };
            $scope.on_select_city = function(val) {
                if ($scope.user_profile == null ){
                    $scope.user_profile = {
                    }
                }
                $scope.user_profile.city = val.id;
            };
        }
    ]);
})(window.angular);
