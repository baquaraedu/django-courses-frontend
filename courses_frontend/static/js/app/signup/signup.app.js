(function(angular){
    'use strict';

    angular.module('signup', [
        'signup.controllers',
    ]);
})(angular);
