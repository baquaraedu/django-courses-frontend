angular.module('baquara').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('en', {"1 comentário":["1 comment",""],"1 gostou":["1 liked",""],"Avisos":"Warnings","Fóruns":"Forums","Repertório pedagógico":"Pedagogical repertoire","Últimas postagens":"Latest posts"});
/* jshint +W100 */
}]);