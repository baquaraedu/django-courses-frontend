var module = angular.module('users.services', []);

class UserService {
    constructor(
        $resource,
        $window,
        $rootScope,
        gettextCatalog,
        UserLocalStorage,
        tmhDynamicLocale,
    ) {
        this.$resource = $resource;
        this.$window = $window;
        this.$rootScope = $rootScope;
        this.gettextCatalog = gettextCatalog;
        this.UserLocalStorage = UserLocalStorage;
        this.tmhDynamicLocale = tmhDynamicLocale;

        this.user = this.api().get_current();
    }

    api () {
        return this.$resource(API_URL + '/users/:userId', {}, {
            get_current: {
                method: 'GET',
                url: API_URL + '/users/current/',
            },
        });
    }
}
UserService.$inject = [
    '$resource',
    '$window',
    '$rootScope',
    'gettextCatalog',
    'UserLocalStorage',

]
module.service('UserService', UserService);
