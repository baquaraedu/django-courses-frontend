var module = angular.module('authorization.services', []);

class AuthorizationService {
    constructor(
        $transitions,
        $window,
        CurrentUser,
    ) {
        this.openRoutes = [
            'home',
            // django urls
            '/accounts/signup/',
            '/accounts/login/',
            '/accounts/password_change/',
            '/accounts/password_change/done/',
            '/accounts/password_reset/',
            '/accounts/password_reset/done/',
            '/accounts/reset/done/',
            '/accounts/',

            // Special cases
            '/pages/',
            '/accounts/reset/',
            '/accounts/recuperar-senha',
            'certificate/',
        ];

        $transitions.onBefore({}, (transition) => {
            let $state = transition.router.stateService;
            const destination = transition.to();
            if (!CurrentUser.is_authenticated) {
                // If the user try to access any URL that not contais openRoutes,
                // the add the redirect parameter and redirect to login page
                if (!this.openRoutes.includes(destination.name)
                            && !this.openRoutes.includes(window.location.pathname)) {
                    // If the user is not authenticated and are in root state,
                    // then go to home state.
                    if (destination.name == 'baquara') {
                        return $state.target('home', undefined, { location: false });
                    } else {
                        $window.location.replace(`/accounts/login?next=/#!${destination.url}`);
                    }
                }
            } else {
                if (destination.name == 'baquara') {
                    return $state.target('dashboard', undefined);
                }
            }
        });
    }
}

AuthorizationService.$inject = [
    '$transitions',
    '$window',
    'CurrentUser',
];

module.service('AuthorizationService', AuthorizationService);
