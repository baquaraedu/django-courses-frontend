(function() {
    'use strict';

    angular.
    module('mainHeader').
    component('mainHeader', {
      templateUrl: '/header.template.html',
      controller: 'HeaderCtrl',
      bindings: {
        anonymous:'<',
      },
    });
})();
