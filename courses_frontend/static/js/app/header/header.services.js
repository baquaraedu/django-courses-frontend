(function(angular){
    'use strict';
    var app = angular.module('header.services', ['ngResource']);

    app.factory('UnreadNotification', ['$resource', function($resource){
        return $resource(BASE_API_URL + '/unread-notification/:id',
            {'id' : '@id'},
            {'update': {'method': 'PUT', 'ignoreLoadingBar': true} });
    }]);

    app.factory('AnswerNotification', ['$resource', function($resource){
        return $resource(BASE_API_URL + '/answer-notification/:id',
            {'id' : '@topic'},
            {'update': {'method': 'PUT', 'ignoreLoadingBar': true} });
    }]);

    app.factory('Notification', ['$resource', function($resource){
        return $resource('/discussion/api/topic-notification/:id',
            {'id' : '@id'},
            {'update': {'method': 'PUT'} });
    }]);
})(window.angular);
