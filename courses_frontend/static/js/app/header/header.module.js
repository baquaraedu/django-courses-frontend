(function() {
    'use strict';

    var app = angular.module('mainHeader', [
        'header.controllers',
        'header.services',
        'header.filters',
        'header.directives',
        'gettext',
        'i18n',
        'djangular',
        'ngAnimate',
        'angular-loading-bar',
        'ui.bootstrap',
        'baquara.services',
        'angular-storage',
    ]);

    app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.spinnerTemplate = '<div class="loading-background"></div><div class="loading-spinner fa fa-spinner fa-pulse fa-4x"></div>';
    }]);
})();
