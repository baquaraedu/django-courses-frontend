(function(angular){
    'use strict';
    var app = angular.module('header.controllers', ['ngCookies']);

    app.controller('HeaderCtrl', [
        '$scope',
        '$rootScope',
        '$window',
        'CurrentUser',
        'UserLocalStorage',
        'TranslationService',
        'UnreadNotification',
        function ($scope, $rootScope, $window, CurrentUser,
            UserLocalStorage, TranslationService, UnreadNotification) {
            var ctrl = this;

            $scope.selectLogo = function (language) {
                switch (language) {
                    case 'pt_br':
                        $scope.logo = "/static/img/logo-pt.png";
                        break;
                    case 'es':
                        $scope.logo = "/static/img/logo-es.png";
                        break;
                    default:
                        $scope.logo = "/static/img/logo-en.png";
                        break;
                }
            };

            ctrl.languageChanged = function(language) {
                $scope.selectLogo(language);
                TranslationService.setLanguage(language, true);
            };

            this.$onInit = function () {

                ctrl.languages = {
                    current: TranslationService.currentLanguage,
                    available: TranslationService.getAvailableLanguages(),
                };

                const resetPaths = [
                    'admin/courses',
                    'admin/course',
                    'pages'
                ];
                const resetHashes = [
                    '#!/category/',
                    '#!/format/',
                    '#!/theme/'
                ];

                // TODO:  why??? whyyyyyyyyyyyyyyyy????!!!
                const pathname = $window.location.pathname;
                const hash = $window.location.hash;
                if (!resetPaths.some((substr) => pathname.includes(substr)) &&
                    !resetHashes.some((substr) => hash.includes(substr))) {
                    if (UserLocalStorage.get('editingCourseLanguage') ||
                        UserLocalStorage.get('editingLanguage') ||
                        UserLocalStorage.get('editingPageLanguage')) {
                        TranslationService.setBackendLanguage(TranslationService.currentLanguage);
                        UserLocalStorage.set('editingLanguage', '');
                        UserLocalStorage.set('editingPageLanguage', '');
                        UserLocalStorage.set('editingCourseLanguage', '');
                        $window.location.reload();
                    }
                }

                $scope.selectLogo(TranslationService.currentLanguage);
            };

            $scope.user = CurrentUser;
            $rootScope.is_main_nav_opened = false;
            $scope.toggle_main_nav_display = function() {
                $rootScope.is_main_nav_opened = !$rootScope.is_main_nav_opened;
            };

            // Start the header with the detailed notifications hidden
            $scope.show_notification = false;

            // Get the unread notifications count for the current user
            if ($scope.user.is_authenticated) {
                UnreadNotification.query(function(unread) {
                    $scope.unread = unread[0];
                    // if there are more than 9 notifications, reduce the number
                    if($scope.unread.counter > 9){
                        $scope.unread.counter = "9+";
                    }
                });
            }

            $scope.toggle_notifications = function(){
                $scope.show_notification = !$scope.show_notification;
                UnreadNotification.update({id: $scope.unread.id, ignoreLoadingBar: true}, function(unread){
                    $scope.unread = unread;
                });
            };
        }
    ]);

})(window.angular);
