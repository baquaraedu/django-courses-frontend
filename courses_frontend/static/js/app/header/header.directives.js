(function (angular) {
  "use strict";
  var app = angular.module("header.directives", []);

  app.directive("notifications", [
    "$q",
    "Notification",
    "AnswerNotification",
    function ($q, Notification, AnswerNotification) {
      return {
        restrict: "E",
        templateUrl: "/static/templates/notifications.html",
        scope: {
          notifications: "=ngModel",
        },
        controller: function ($scope) {
          // Normalize all the diferent notification types for easy use in the template
          $scope.parsed_notifications = [];
          $scope.notifications_ready = false;
          
          if (!$scope.notifications) {
            // Both notification sources must resolve before $scope.parsed_notifications can be created
            $q.all({
              discussion: Notification.query({ limit_to: 10 }).$promise,
              answer: AnswerNotification.query({ limit_to: 10 }).$promise,
            })
              .then((notifications) => {
                // Parse notifications from discussion app
                for (let i = 0; i < notifications.discussion.length; i++) {
                  // Defining consts
                  $scope.parsed_notifications[i] = {};
                  const notification = notifications.discussion[i];
                  const parsedNotification = $scope.parsed_notifications[i];
                  
                  // Fill parsedNotification with data from query
                  parsedNotification.action = notification.action;
                  parsedNotification.date = notification.date;
                  parsedNotification.topic = notification.topic;
                  parsedNotification.forum = notification.topic.forum;
                  parsedNotification.is_read = notification.is_read;

                  // Fill user data in parsedNotification
                  switch (notification.action) {
                    case "new_topic":
                      parsedNotification.user = notification.topic.author;
                      break;
                    case "new_comment":
                      parsedNotification.user = notification.comment.author;
                      break;
                    case "new_reaction":
                      parsedNotification.user = notification.topic_like.user;
                      break;
                    case "new_reaction_comment":
                      parsedNotification.user = notification.comment_like.user;
                      break;
                    default:
                      parsedNotification = notification;
                  }
                } // End For
                
                // Include notifications from activities answers (if any)
                $scope.parsed_notifications = $scope.parsed_notifications.concat(
                  notifications.answer
                );
              })
              .then(() => {
                // Notifications are ready to be shown, so the template must be alerted
                $scope.notifications_ready = true;
              })
              .catch((err) => {
                console.error(err);
              });
          }
        },
      };
    },
  ]);
})(window.angular);
