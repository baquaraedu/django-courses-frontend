(function() {
  'use strict';

  angular.
  module('mainHeaderUnlogged').
  component('mainHeaderUnlogged', {
    templateUrl: '/header-unlogged.template.html',
    controller: 'HeaderCtrl',
    bindings: {
      anonymous:'<',
    },
  });
})();
