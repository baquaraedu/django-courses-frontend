(function() {
    'use strict';

    angular.
    module('generalReports').
    component('generalReports', {
      templateUrl: '/general-reports.template.html',
    });
})();
