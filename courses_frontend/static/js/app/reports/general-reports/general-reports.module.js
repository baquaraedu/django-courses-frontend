(function(angular){
    'use strict';

    var app = angular.module('generalReports', [
        'reports.services',
    ]);

    app.controller('GeneralReportsCtrl', [
        '$scope',
        'GeneralSummary',
        'Group',
        'BasicClass',
        function(
            $scope,
            GeneralSummary,
            Group,
            BasicClass
        ) {
            $scope.general_data = GeneralSummary.get({}, function() {}, function(error) {
                // alert('Não é possível exportar relatórios agora. Por favor, converse com o suporte');
                // window.open('/dashboard', '_self');
            });
            // $scope.contracts = Contract.query();

            // General report for dowload
            $scope.general_report = {};
            
            $scope.groups = Group.query({});

            $scope.classes = BasicClass.query({});

            
            $scope.download_general_report = function() {
                var options = '?';
                for(var idx in $scope.general_report.groups) {
                    if(idx == 0)
                        options += ('group=' + $scope.general_report.groups[0].name);
                    else
                        options += (',' + $scope.general_report.groups[idx].name);
                }
                window.open('/legacy/api/users-by-group' + options + '&format=csv','_blank');
            };

            // Course report for download
            $scope.course_report = {};
            $scope.download_course_report = function() {
                var options = '?';
                for(var idx in $scope.course_report.classes) {
                    if(idx == 0)
                        options += ('id=' + $scope.course_report.classes[0].id);
                    else
                        options += (',' + $scope.course_report.classes[idx].id);
                }
                window.open('/legacy/api/users-by-class' + options + '&format=csv','_blank');
            }
        }
    ]);
})(angular);
