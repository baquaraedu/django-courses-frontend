(function (angular) {
    'use strict';
    /* Services */
    angular.module('reports.services', ['ngResource']).
        factory('CourseUserReport', function($resource){
            return $resource(BASE_API_URL + '/reports', {}, {
            });
        }).factory('ActionsUserReport', function($resource){
            return $resource(COURSES_BASE_API_URL + '/user-activities', {}, {
            });
        }).factory('AccessUserReport', function($resource){
            return $resource(COURSES_BASE_API_URL + '/user-access', {}, {
            });
        }).factory('LessonsUserProgress', function($resource){
            return $resource(BASE_API_URL + '/lessons_user_progress/:courseId', {}, {
            });
        }).factory('CourseStats', function($resource){
            return $resource(BASE_API_URL + '/course_stats/:courseId', {}, {
            });
        }).factory('GeneralSummary', function($resource){
            return $resource(BASE_API_URL + '/summary/', {}, {
            });
        }).factory('Contract', function($resource){
            return $resource(BASE_API_URL + '/contract/', {}, {
            });
        }).factory('GeneralUserReports', function($resource){
            return $resource(COURSES_BASE_API_URL + '/user-reports/', {}, {
            });
        }).factory('GeneralFilterOptions', function($resource){
            return $resource(COURSES_BASE_API_URL + '/report-filters/', {}, {
            });
        }).factory('UserAccess', function($resource){
            return $resource(API_URL + '/stats/user-access', {}, {'save': {'method': 'POST', 'ignoreLoadingBar': true}
            });
        }).factory('UserActions', function($resource){
            return $resource(API_URL + '/stats/user-actions', {}, {'save': {'method': 'POST', 'ignoreLoadingBar': true}
            });
        });
})(angular)