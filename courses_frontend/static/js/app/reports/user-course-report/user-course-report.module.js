(function() {
    'use strict';

    angular.module('userCourseReport', [
        'reports.services',
    ]);

})();
