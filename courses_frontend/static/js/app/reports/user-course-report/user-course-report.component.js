(function() {
    'use strict';

    angular
        .module('userCourseReport')
        .component('userCourseReport', {
            templateUrl: '/user-course-report.template.html',
            controller: UserCourseReportCtrl,
            bindings: {
                course: '<',
                user: '<',
            },
        });

    UserCourseReportCtrl.$inject = ['$scope', 'LessonsUserProgress'];

    function UserCourseReportCtrl ($scope, LessonsUserProgress) {
        var ctrl = this;

        ctrl.lessons = [];
        ctrl.showDetails = false;
        ctrl.toggleDetails = toggleDetails;

        ctrl.$onInit = function() {

        };

        function toggleDetails() {
            ctrl.showDetails = !ctrl.showDetails;

            if (ctrl.showDetails && ctrl.lessons.length === 0) {
                LessonsUserProgress.get({ courseId: ctrl.course.course_id, user: ctrl.user.id }, (lessons) => {
                    ctrl.lessons = lessons.lessons_progress;
                });
            }
        }
    }
})();
