(function() {
    'use strict';
    angular.
        module('userReports').
        component('userReports', {
            templateUrl: '/user-reports.template.html',
            controller: UserReportsCtrl,
            bindings: {
            },
        });

    UserReportsCtrl.$inject = ['$scope', 'GeneralUserReports'];

    function UserReportsCtrl($scope, GeneralUserReports){

        var ctrl = this;

        ctrl.city = '';
        ctrl.fetchKeyword = throttleDebounce.debounce(1000, fetchKeyword);
        ctrl.classroom = '';
        ctrl.filters = null;
        ctrl.search = '';
        ctrl.filteredUsers = [];
        ctrl.nte = '';
        ctrl.page = null;
        ctrl.paginatedUsers = [];
        ctrl.role = '';
        ctrl.users = [];
        ctrl.totalUsers = 0;
        ctrl.totalPage = 0;

        ctrl.old = { city: '', classroom: '', search: '', nte: '', role: '' };

        ctrl.$onChanges = function() {
            fetchKeyword();
        };

        ctrl.$onInit = function() {
            fetchUserReportPage();
        };

        function fetchKeyword() {
            fetchUserReportPage(1)
        }

        ctrl.$doCheck = function() {
            if (ctrl.city !== ctrl.old.city || ctrl.classroom !== ctrl.old.classroom || ctrl.nte !== ctrl.old.nte || ctrl.role !== ctrl.old.role) {
                fetchUserReportPage(1);
            }
            if (ctrl.users !== ctrl.old.users) {
                paginateUsers();
            }
            ctrl.old.city = ctrl.city;
            ctrl.old.classroom = ctrl.classroom;
            ctrl.old.search = ctrl.search;
            ctrl.old.nte = ctrl.nte;
            ctrl.old.page = ctrl.page;
            ctrl.old.role = ctrl.role;
            ctrl.old.users = ctrl.users;
        };
           
        function fetchUserReportPage(page) {
            const params = { page: page };
            let filteredRequest = false;

            if (ctrl.city) {
                params.municipio = ctrl.city;
            }
            if (ctrl.classroom) {
                params.turma = ctrl.classroom;
            }
            if (ctrl.nte) {
                params.nte = ctrl.nte;
            }
            if (ctrl.role) {
                params.funcao = ctrl.role;
            }
            if (ctrl.search) {
                params.search = ctrl.search;
            }

            GeneralUserReports.get(params, (data) => {
                ctrl.users = data.results;
                ctrl.filteredUsers = ctrl.users;
                ctrl.totalUsers = data.count;
                ctrl.page = page;
            });
        }

        function paginateUsers() {
            ctrl.paginatedUsers = ctrl.filteredUsers;

            if (ctrl.page > 1) {
                ctrl.totalPage = ctrl.paginatedUsers.length + ((ctrl.page - 1) * 50);
            } else {
                ctrl.totalPage = ctrl.paginatedUsers.length;
            }
        }

        $scope.filter_page = function() {
            fetchUserReportPage(ctrl.page);
        };
    }
})();
