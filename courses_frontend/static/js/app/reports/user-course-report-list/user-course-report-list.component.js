(function() {
    'use strict';

    angular
        .module('userCourseReportList')
        .component('userCourseReportList', {
            templateUrl: '/user-course-report-list.template.html',
            controller: UserCourseReportListCtrl,
            bindings: {
                user: '<',
            },
        });

    UserCourseReportListCtrl.$inject = ['$scope', 'CourseUserReport'];

    function UserCourseReportListCtrl ($scope, CourseUserReport) {

        var ctrl = this;

        ctrl.courses = [];

        ctrl.$onInit = function() {
            CourseUserReport.get({ user: ctrl.user.id }, (data) => {
                ctrl.courses = data.results.sort((a, b) => a.course_name > b.course_name ? 1 : -1);
            });
        };
    }
})();
