(function() {
    'use strict';

    angular.module('userCourseReportList', [
        'reports.services',
    ]);

})();
