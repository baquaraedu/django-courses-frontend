(function() {
    'use strict';

    angular
        .module('userActionsReport')
        .component('userActionsReport', {
            templateUrl: '/user-actions-report.template.html',
            controller: UserCourseReportListCtrl,
            bindings: {
                user: '<',
            },
        });

    UserCourseReportListCtrl.$inject = ['$scope', 'ActionsUserReport'];

    function UserCourseReportListCtrl ($scope, ActionsUserReport) {
        var ctrl = this;

        ctrl.courses = [];
        ctrl.page = 1

        ctrl.$onInit = function() {
            ActionsUserReport.get({ user: ctrl.user.id }, (data) => {
                ctrl.actions = data.results;
                ctrl.totalObjects = data.count;
            });
        };

        $scope.limit = 10;

        $scope.loadMore = function() {
            if ($scope.limit < ctrl.totalObjects) {
                $scope.limit = $scope.limit + 10;
            }
            var next_page = parseInt($scope.limit/30) + 1;

            if (next_page !== ctrl.page) {
                fetchUserActivitiesPage(next_page);
                ctrl.page = next_page;
            }
        }

        $scope.download_actions = function() {
            var options = '?';
            options += ('user=' + ctrl.user.id);
            window.open('/api/actions-user' + options + '&format=xlsx','_blank');
        }

        function fetchUserActivitiesPage(page) {
            ActionsUserReport.get({ user: ctrl.user.id, page }, (data) => {
                if (ctrl.actions !== undefined) {
                    ctrl.actions.concat(data.results);
                }
            });
        }
    }
})();
