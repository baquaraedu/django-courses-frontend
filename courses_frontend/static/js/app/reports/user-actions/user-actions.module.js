(function() {
    'use strict';

    angular.module('userActionsReport', [
        'reports.services',
    ]);

})();
