(function() {
    'use strict';

    angular
        .module('userReport')
        .component('userReport', {
            templateUrl: '/single-user-report.template.html',
            controller: UserReportCtrl,
            bindings: {
                user: '<',
            },
        });

    UserReportCtrl.$inject = ['$scope'];

    function UserReportCtrl ($scope) {
        var ctrl = this;

        ctrl.showDetails = false;
        ctrl.toggleDetails = toggleDetails;

        ctrl.$onInit = function() {};

        function toggleDetails() {
            ctrl.showDetails = !ctrl.showDetails;
        }
    }
})();
