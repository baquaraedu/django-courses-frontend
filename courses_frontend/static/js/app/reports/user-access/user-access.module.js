(function() {
    'use strict';

    angular.module('userAccessReport', [
        'reports.services',
    ]);

})();
