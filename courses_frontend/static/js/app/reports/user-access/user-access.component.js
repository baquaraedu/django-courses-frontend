(function() {
    'use strict';

    angular
        .module('userAccessReport')
        .component('userAccessReport', {
            templateUrl: '/user-access-report.template.html',
            controller: UserAccessCtrl,
            bindings: {
                user: '<',
            },
        });

    UserAccessCtrl.$inject = ['$scope', 'AccessUserReport'];

    function UserAccessCtrl ($scope, AccessUserReport) {

        var ctrl = this;

        ctrl.access = {};

        ctrl.$onInit = function() {
            AccessUserReport.get({ user: ctrl.user.id }, (data) => {
                ctrl.last_access = data.last_access;
                ctrl.last_completed_activity = data.last_completed_activity;
                ctrl.accesses_7_days = data.accesses_7_days;
                ctrl.accesses_30_days = data.accesses_30_days;
                ctrl.last_login = data.last_login;
            });
        };
    }
})();
