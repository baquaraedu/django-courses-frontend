(function (angular) {
    'use strict';

    var module = angular.module('messages.services', ['ngResource']);
    module.factory('Message', function($resource){
        return $resource(API_URL + '/message/:messageId', {messageId: '@id'}, {
            update: {method: 'PUT'},
            send: {
                method: 'GET',
                url: API_URL + '/message/:messageId/send',
                // params: {messageId: '@id'}},
            },
        });
    });

    module.factory('MessageBasic', function($resource){
        return $resource(API_URL + '/message_basic/:messageId', {messageId: '@id'}, {
            unread_count: {
                method: 'GET',
                url: API_URL + '/message_basic/unread_count',
            },
        });
    });

    module.factory('MessageRead', function($resource){

        // The paramiter here is the id of the related message, not the MessageRead objetc
        return $resource(API_URL + '/message_read/:messageId', {message: '@message'}, {
        });
    });

    class MessagesEvents {
        constructor(
            $rootScope,
        ) {
            this.$rootScope = $rootScope;
        }

        updated(message) {
            this.$rootScope.$broadcast('messageUpdated', message);
        }
    }

    MessagesEvents.$inject = [
        '$rootScope',
    ]

    module.service('MessagesEvents', MessagesEvents)

})(angular);
