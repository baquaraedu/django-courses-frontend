(function() {
    'use strict';
    angular.
        module('messagesList').
        component('messagesList', {
            templateUrl: '/messages-list.template.html',
            controller: MessagesListCtrl,
            bindings: {
                messages: '<',
                listTitle: '<',
            },
        });

    // MessagesListCtrl.$inject = [
    //     '$scope',
    //     '$routeParams',
    //     '$stateParams',
    // ];

    function MessagesListCtrl (
        // $scope, $routeParams, $stateParams,
    ) {

    }
})();