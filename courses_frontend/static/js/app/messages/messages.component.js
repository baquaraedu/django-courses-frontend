(function() {
    'use strict';
    angular.
        module('messages').
        component('messages', {
            templateUrl: '/messages.template.html',
            controller: MessagesCtrl,
        });

    MessagesCtrl.$inject = [
        '$rootScope',
        '$stateParams',
        '$window',
        'User',
        'UserAccess',
        'Message',
        'MessageBasic',
    ];

    function MessagesCtrl (
        $rootScope,
        $stateParams,
        $window,
        User,
        UserAccess,
        Message,
        MessageBasic,
    ) {
        var ctrl = this;

        ctrl.user = null;

        // const courseSlug = $stateParams.courseSlug;
        const courseId = $window.course_id;

        function fetchMessages (user) {
            ctrl.my_messages = MessageBasic.query({my_messages: true, course: courseId});

            if (user.is_superuser) {
                ctrl.published_messages = Message.query({status:'published', course: courseId});
                ctrl.draft_messages = Message.query({status:'draft',  course: courseId});
                ctrl.welcome_messages = Message.query({type:'welcome'});
            }
        }

        $rootScope.$on('languageChanged', () => {
            fetchMessages(ctrl.user);
        });

        $rootScope.$on('messageUpdated', () => {
            fetchMessages(ctrl.user);
        });

        ctrl.$onInit = function() {
            User.get_current((user) => {
                ctrl.user = user;
                fetchMessages(user);
            });

            // Log this access
            (new UserAccess({ area: 'messages' })).$save().then(
                (success) => {},
                (error) => {
                    console.warn(`The area: 'messages' was not registered in UserAccess`)
                }
            );
        }
    }
})();
