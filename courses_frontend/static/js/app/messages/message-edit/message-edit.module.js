(function() {
    'use strict';

    angular.module('messageEdit', [
        'moment-picker',
        'ang-dialogs',
    ]);

})();