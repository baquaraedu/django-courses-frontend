(function() {
    'use strict';
    angular.
        module('messageEdit').
        component('messageEdit', {
            templateUrl: '/messages-edit.template.html',
            controller: MessageEditCtrl,
            bindings: {
                message: '<',
            },
        });

    MessageEditCtrl.$inject = [
        '$state',
        '$stateParams',
        '$window',
        '$scope',
        '$dialogs',
        'TranslationService',
        'gettextCatalog',
        'Message',
        'MessagesEvents',
        'User',
        'Group',
        'UserSearch',
        'BasicClass',
        'CourseProfessor',
        'ContentFile',
        'uiTinymceConfig',
    ];

    function MessageEditCtrl (
        $state,
        $stateParams,
        $window,
        $scope,
        $dialogs,
        TranslationService,
        gettextCatalog,
        Message,
        MessagesEvents,
        User,
        Group,
        UserSearch,
        BasicClass,
        CourseProfessor,
        ContentFile,
        uiTinymceConfig,
    ) {

        var ctrl = this;
        const messageId = $stateParams.messageId;
        const courseId = $window.course_id;
        uiTinymceConfig.images_upload_handler = ContentFile.upload;

        this.$onInit = function() {

            ctrl.schedule = {};
            ctrl.schedule.currentLanguage = TranslationService.currentLanguage;
            ctrl.schedule.now = moment().add(1, 'hours').locale(TranslationService.currentLanguage);

            ctrl.recipient_list = [];
            ctrl.empty_msg_subject_error = false;
            ctrl.empty_msg_body_error = false;

            ctrl.alerts = [];

            if (!ctrl.message && messageId) {
                ctrl.new_message = Message.get({messageId: messageId}, function (message) {
                    if (message.release_date) {
                        ctrl.schedule.release_date_obj = moment(message.release_date).locale(TranslationService.currentLanguage);
                        ctrl.schedule_message = true;
                    }
                    ctrl.recipient_list = message.recipients_obj;
                    ctrl.originalMessage = angular.copy(message);
                    return message;
                });
            } else {
                // New message
                ctrl.new_message = new Message();
                // ctrl.new_message.users = [];

                if (courseId) {
                    ctrl.new_message.course = courseId;
                    ctrl.is_course_broadcast = true;
                }
                ctrl.originalMessage = angular.copy(ctrl.new_message);

            }


            ctrl.user = User.get_current();
            Group.query({}, function (groups) {
                ctrl.groups = groups;
            });

            if (courseId) {
                CourseProfessor.query({
                    course: courseId,
                    user: ctrl.user.id
                }, function(course_professor) {
                    if (Array.isArray(course_professor) && course_professor.length) {
                        ctrl.user.course_role = course_professor[0].role;
                    }
                });

                ctrl.classes = BasicClass.query({
                    course: courseId
                }, function(classes) {
                    classes.sort((a,b) => {
                        return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1
                    })
                    return classes;
                });
            }
        }


        this.$postLink = function() {
            $scope.$on('languageChanged', function(event, data) {
                ctrl.schedule.currentLanguage = data.newLanguage;
            });
        }

        this.closeAlert = function(index) {
            ctrl.alerts.splice(index, 1);
        };


        ctrl.uiCanExit = function() {
            if (ctrl.canExit || angular.equals(ctrl.new_message, ctrl.originalMessage)) {
              return true;
            }
            $dialogs.showConfirmationDialog(
                gettextCatalog.getString("If you leave, there are unsaved changes that will be lost!"),
                {
                    title: gettextCatalog.getString("Leave whithout saving and discard changes?"),
                    closable: true,
                    callback: function(option) {
                        if(option === "ok") {
                            ctrl.canExit = true;
                            $state.go("^", null, { reload: true });
                        }
                    }
                }
            );
            return false;
        }

        ctrl.validate = function () {
            const empty_msg_body_error_en = !ctrl.new_message.message_en ? true : false;
            const empty_msg_subject_error_en = !ctrl.new_message.subject_en ? true : false;

            const empty_msg_body_error_es = !ctrl.new_message.message_es ? true : false;
            const empty_msg_subject_error_es = !ctrl.new_message.subject_es ? true : false;

            const empty_msg_body_error_pt_br = !ctrl.new_message.message_pt_br ? true : false;
            const empty_msg_subject_error_pt_br = !ctrl.new_message.subject_pt_br ? true : false;

            ctrl.empty_msg_check_options_error = false;

            ctrl.empty_msg_subject_error = (empty_msg_subject_error_en &&
                empty_msg_subject_error_es && empty_msg_subject_error_pt_br);
            ctrl.empty_msg_body_error = (empty_msg_body_error_en &&
                empty_msg_body_error_es && empty_msg_body_error_pt_br);

            if (ctrl.empty_msg_subject_error === false && ctrl.empty_msg_body_error === false) {
                if (empty_msg_body_error_en === false && empty_msg_subject_error_en === false) {
                    ctrl.new_message.message = ctrl.new_message.message_en;
                    ctrl.new_message.subject = ctrl.new_message.subject_en;
                } else if (empty_msg_body_error_es === false && empty_msg_subject_error_es === false) {
                    ctrl.new_message.message = ctrl.new_message.message_es;
                    ctrl.new_message.subject = ctrl.new_message.subject_es;
                } else if (empty_msg_body_error_pt_br === false && empty_msg_subject_error_pt_br === false) {
                    ctrl.new_message.message = ctrl.new_message.message_pt_br;
                    ctrl.new_message.subject = ctrl.new_message.subject_pt_br;
                }
            }

            // TODO: refactor validation to suport internacionalization
            if (ctrl.new_message.course) {
                if (ctrl.is_course_broadcast) {
                    ctrl.classes.map((klass) => {
                        ctrl.new_message.classes.push(klass.id);
                    });
                } else {
                    ctrl.empty_msg_recipients_check_classes_error = ctrl.new_message.classes ? ctrl.new_message.classes.length === 0 : true;
                    ctrl.empty_msg_recipients_check_users_options_error = ctrl.new_message.recipients ? ctrl.new_message.recipients.length === 0 : true;

                    if (ctrl.empty_msg_recipients_check_classes_error && ctrl.empty_msg_recipients_check_users_options_error) {
                        ctrl.text_msg_check_options_error = 'um destinatário';
                    }
                }
            } else if (!ctrl.new_message.is_broadcast) {
                ctrl.empty_msg_recipients_check_groups_error = ctrl.new_message.groups ? ctrl.new_message.groups.length === 0 : true;
                ctrl.empty_msg_recipients_check_users_options_error = ctrl.new_message.recipients ? ctrl.new_message.recipients.length === 0 : true;

                if (ctrl.empty_msg_recipients_check_groups_error && ctrl.empty_msg_recipients_check_users_options_error) {
                    ctrl.text_msg_check_options_error = 'um destinatário';
                }
            }

            if (ctrl.new_message.is_active_welcome ||
                    (!ctrl.empty_msg_body_error && !ctrl.empty_msg_subject_error && !ctrl.empty_msg_check_options_error)
            ) {
                ctrl.empty_msg_subject_error = false;
                ctrl.empty_msg_body_error = false;
                ctrl.empty_msg_check_options_error = false;
                return true;
            }
        }

        ctrl.save = function () {

            if (ctrl.validate()) {
                ctrl.canExit = true;
                if (!ctrl.schedule_message) {
                    ctrl.new_message.release_date = null;
                } else if (ctrl.schedule.release_date_obj) {
                    ctrl.new_message.release_date = ctrl.schedule.release_date_obj.minute(0).second(0).format();
                }
                if (ctrl.new_message.is_active_welcome) {
                    ctrl.new_message.type = 'welcome';
                }

                let messagePromisse;
                if (ctrl.new_message.id)
                    messagePromisse = ctrl.new_message.$update();
                else
                    messagePromisse = ctrl.new_message.$save();
                messagePromisse.then(
                    () => {
                        MessagesEvents.updated(ctrl.new_message);
                        ctrl.alerts.push({msg: gettextCatalog.getString("Announcement successfully saved!")});
                    },
                    () => ctrl.alerts.push({msg: gettextCatalog.getString("Error saving announcement!")}),
                );
                return messagePromisse;
            }
        };

        ctrl.send = function () {

            if (ctrl.validate()) {
                    ctrl.save().then(function(message) {
                        ctrl.new_message.$send(function(message) {
                            $dialogs.showSuccessDialog(gettextCatalog.getString("Announcement successfully sent."),
                            {
                                closeTimeout: 5
                            });
                            MessagesEvents.updated(message);
                            ctrl.canExit = true;
                            $state.go("^", null, { reload: true });
                        }, function(error) {
                            $dialogs.showErrorDialog(error.data.error, {
                                title: gettextCatalog.getString("There was an error sending this announcement.")
                            });
                        });
                    });
            }
        };

        ctrl.cancel = function () {
            $state.go("^", null, { reload: true });
        };

        ctrl.delete = function () {
            $dialogs.showConfirmationDialog(gettextCatalog.getString("If you delete this announcement it cannot be recovered!"), {
                title: gettextCatalog.getString("Delete this announcement?"),
                closable: true,
                buttonOkText: "Delete",
                callback: function(option) {
                    if(option === "ok") {
                        ctrl.new_message.$delete();
                        ctrl.canExit = true;
                        $state.go("^.^", null, { reload: true });
                    }
                }
            });
        };

        ctrl.getUsers = function(val) {
            return new UserSearch(val);
        };

        ctrl.group_label = function(item) {
            if (!item) return
            else if (item.workspace && item.workspace.name != '')
                return item.name + ' - ' + item.workspace.name;
            else
                return item.name;
        };

        ctrl.on_select_student = function(model) {
            if (!ctrl.new_message.recipients) {
                ctrl.new_message.recipients = [];
            }
            ctrl.new_message.recipients.unshift(model.id);
            ctrl.recipient_list.unshift(model);
            ctrl.asyncSelected = '';
        };

        ctrl.remove_student = function(index) {
            if (!ctrl.new_message.recipients) {
                ctrl.new_message.recipients = [];
            }
            ctrl.new_message.recipients.splice(index, 1);
            ctrl.recipient_list.splice(index, 1);
        };
    }
})();
