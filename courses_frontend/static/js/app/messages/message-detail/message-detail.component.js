(function() {
    'use strict';
    angular.
        module('messageDetail').
        component('messageDetail', {
            templateUrl: '/messages-detail.template.html',
            controller: MessageDetailCtrl,
            bindings: {
                message: '<',
            },
        });

    MessageDetailCtrl.$inject = [
        '$stateParams',
        '$window',
        '$scope',
        '$rootScope',
        'MessageBasic',
        'Message',
        'MessagesEvents',
        'MessageRead',
        'User',
        'CourseProfessor',
    ];

    function MessageDetailCtrl (
        $stateParams,
        $window,
        $scope,
        $rootScope,
        MessageBasic,
        Message,
        MessagesEvents,
        MessageRead,
        User,
        CourseProfessor,
    ) {

        var ctrl = this;
        const messageId = $stateParams.messageId;
        const courseId = $window.course_id;

        ctrl.fetchMessage = () => {
            ctrl.message = MessageBasic.get({messageId: messageId}, function(message) {
                if (message.status == 'published') {
                    MessageRead.get({messageId: messageId}, (response) => {
                        if (response.is_read)
                            return response
                    }, () => {
                        MessageRead.save({message: messageId}, function(message_read){
                            $rootScope.$broadcast('announcementRead', {});
                        });
                    })
                }
                return message;
            });
        }

        this.$onInit = function() {
            ctrl.fetchMessage();

            $scope.$on('languageChanged', () => {
                ctrl.fetchMessage();
            })

            ctrl.user = User.get_current();
            if (courseId) {
                CourseProfessor.query({
                    course: courseId,
                    user: ctrl.user.id
                }, function(course_professor) {
                    if (Array.isArray(course_professor) && course_professor.length) {
                        ctrl.user.course_role = course_professor[0].role;
                    }
                });
            }
        }

        this.toogleWelcomeMessage = function () {
            ctrl.message.is_active_welcome = !ctrl.message.is_active_welcome;
            Message.update(ctrl.message, function(message) {
                MessagesEvents.updated(message);
            });
        }
    }
})();
