(function() {
    'use strict';

    var app = angular.module('messages', [
        'ui.router',
        'oc.lazyLoad',
        'ngSanitize',
        'ui.select',
        'angular.filter',
        'checklist-model',
        'ngFileUpload',
        // Fixme avoid call shared here, it is need couse message module é calls inside
        // course message, that don't use app baquara, but messages direct
        'shared',
    ]);

    app.config(function($stateProvider) {
        $stateProvider
        .state('messages', {
            url: '/messages',
            component: 'messages',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                    '/static/js/app/messages/messages.component.js',
                    '/static/js/app/messages/messages-list/messages-list.module.js',
                    '/static/js/app/messages/messages-list/messages-list.component.js',
                    // Services dependencies
                    '/static/js/app/messages/messages.services.js',
                    '/static/js/app/courses/courses.services.js',
                    '/static/js/app/accounts/accounts.services.js',
                    '/static/js/app/reports/reports.services.js',
                  ],
                  {
                    serie: true,
                  }
                );
              }]
            },
            deepStateRedirect: true,
            sticky: true,
        })
        .state({
            name: 'messages.message',
            url: '/:messageId',
            component: 'messageDetail',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                    '/static/js/app/messages/message-detail/message-detail.module.js',
                    '/static/js/app/messages/message-detail/message-detail.component.js',
                    // Services dependencies
                    '/static/js/app/messages/messages.services.js',
                    '/static/js/app/courses/courses.services.js',
                    '/static/js/app/accounts/accounts.services.js',
                    '/static/js/app/reports/reports.services.js',
                  ],
                  {serie: true}
                );
              }]
            },
        })
        .state({
            name: 'messages.message.edit',
            url: '/edit',
            // component: 'messageEdit',
            resolve: {
              lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load(
                  [
                    'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
                    'https://unpkg.com/moment-timezone@0.5.34/builds/moment-timezone-with-data-10-year-range.min.js',
                    'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.js',
                    'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.css',
                    '/static/vendor/angular-dialogs/angular-dialogs.min.js',

                    '/static/js/app/messages/message-edit/message-edit.module.js',
                    '/static/js/app/messages/message-edit/message-edit.component.js',
                    
                    // Services dependencies
                    '/static/js/ui.tinymce.config.js',
                    '/static/js/app/messages/messages.services.js',
                    '/static/js/app/courses/courses.services.js',
                    '/static/js/app/accounts/accounts.services.js',
                    '/static/js/app/shared/shared.services.js',
                  ],
                  {serie: true}
                );
              }]
            },
            views: {
              // Relatively target the grand-parent-state's $default (unnamed) ui-view
              // This could also have been written using ui-view@state addressing: $default@messages
              // Or, this could also have been written using absolute ui-view addressing: !$default.$default.$default
              '^.^.$default': {
                // bindings: { pristineContact: "contact" },
                component: 'messageEdit'
              },
            },
        })
        .state({
          name: 'messages.new',
          url: '/new',
          component: 'messageEdit',
          resolve: {
            lazyLoadModule: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load(
                [
                  'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
                  'https://unpkg.com/moment-timezone@0.5.34/builds/moment-timezone-with-data-10-year-range.min.js',
                  'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.js',
                  'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.css',
                  '/static/vendor/angular-dialogs/angular-dialogs.min.js',

                  '/static/js/app/messages/message-edit/message-edit.module.js',
                  '/static/js/app/messages/message-edit/message-edit.component.js',
                  
                  // Services dependencies
                  '/static/js/ui.tinymce.config.js',
                  '/static/js/app/messages/messages.services.js',
                  '/static/js/app/courses/courses.services.js',
                  '/static/js/app/accounts/accounts.services.js',
                  '/static/js/app/shared/shared.services.js',
                ],
                {serie: true}
              );
            }]
          },
      })
    })
})();