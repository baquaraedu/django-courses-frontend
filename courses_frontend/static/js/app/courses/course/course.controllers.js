(function(angular){
    'use strict';
    var app = angular.module('course.controllers', []);

    app.controller('CourseCtrl', [
        '$scope',
        '$stateParams',
        'CourseBySlug',
        'CourseLessons',
        function ($scope, $stateParams, CourseBySlug, CourseLessons) {
            var ctrl = this;
            $scope.course = {};
            
            ctrl.in_admin = false;

            ctrl.$onInit = function() {
                let courseSlug = $stateParams.courseSlug;

                CourseBySlug.get({slug: courseSlug}, (course) => {
                    $scope.course = course;
                    let youtube_video_id = (course.intro_video? course.intro_video.youtube_id : '');
                    $scope.youtube_video_url = "//www.youtube.com/embed/" + youtube_video_id  + "?rel=0&showinfo=0&autohide=1&theme=light&wmode=opaque";

                    CourseLessons.query({course_id: course.id}, (lessons) => {
                        $scope.lessons = lessons
                    });
                });
            }
        }
    ]);

})(window.angular);
