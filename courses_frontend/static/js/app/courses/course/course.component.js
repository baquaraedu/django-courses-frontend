(function() {
    'use strict';

    angular.
    module('course').
    component('course', {
      templateUrl: '/course.template.html',
    });
})();
