(function(angular){
    'use strict';

    var app = angular.module('courses', [
        'courses.controllers',
        'core.services',
        'django',
        'timtec-models',
        'directive.file',
        'ui.bootstrap',
        'ngRoute',
        'ngResource',
        //'shared',
    ]);
})(angular);
