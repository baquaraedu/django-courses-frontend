(function() {
    'use strict';

    angular.
    module('courses').
    component('courses', {
      templateUrl: '/courses.template.html',
      controller: 'CourseListByPositionController',
    });
})();
