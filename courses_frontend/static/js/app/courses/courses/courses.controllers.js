(function(angular){
    'use strict';
    var app = angular.module('courses.controllers', []);

    app.controller('CourseListByPositionController', [
        '$scope',
        'Course',
        'CourseStudent',
        'MyCourses',
        function (
            $scope,
            Course,
            CourseStudent,
            MyCourses,
        ) {
            function compare_by_position(a,b) {
                if (a.home_position !== null && b.home_position !== null) {
                    if (a.home_position < b.home_position)
                       return -1;
                    if (a.home_position > b.home_position)
                       return 1;
                } else if (a.home_position !== null && b.home_position === null)
                    return -1;
                else if (a.home_position === null && b.home_position !== null)
                    return 1;
                return 0;
            }

            MyCourses.query({}, function (my_courses) {
                $scope.my_courses = my_courses;

                CourseStudent.query({}, function (course_students){
                    $scope.course_students = course_students
                    angular.forEach($scope.my_courses, function(my_course) {
                        $scope.langs = []
                        angular.forEach(my_course.lang, function(lang) {
                            let langs_dict = {
                                'en': 'English',
                                'es': 'Spanish',
                                'pt-br': 'Portuguese'
                            }
                            $scope.langs.push(langs_dict[lang]);
                        });
                        my_course.lang = $scope.langs
                        angular.forEach($scope.course_students, function(course_student) {
                            if (my_course.id === course_student.course.id) {
                                my_course.course_student = course_student;
                            }
                        });
                    });
                });
            });

            Course.query({}, function (courses) {
                courses.sort(compare_by_position);
                $scope.courses = courses;
                CourseStudent.query({}, function (course_students){
                    $scope.course_students = course_students
                    angular.forEach($scope.courses, function(course) {
                        $scope.langs = []
                        angular.forEach(course.lang, function(lang) {
                            let langs_dict = {
                                'en': 'English',
                                'es': 'Spanish',
                                'pt-br': 'Portuguese'
                            }
                            $scope.langs.push(langs_dict[lang]);
                        });
                        course.lang = $scope.langs
                        angular.forEach($scope.course_students, function(course_student) {
                            if (course.id === course_student.course.id) {
                                course.course_student = course_student;
                            }
                        });
                    });
                });

                let coursesByTrack = {}
                angular.forEach(courses, function(course) {
                    let trackName = course.track.name;
                    if (trackName === undefined) {
                        trackName = 'no track';
                    }

                    if (!(trackName in coursesByTrack)) {
                        coursesByTrack[trackName] = {'courses': [], 'home_position': null};
                    }

                    coursesByTrack[trackName]['courses'].push(course);
                    let position = (course.track.home_position !== undefined ? course.track.home_position : null);
                    coursesByTrack[trackName]['home_position'] = position;
                });

                $scope.coursesByTrack = Object.keys(coursesByTrack).map(function(key) {
                    return {'name': key, 'courses': coursesByTrack[key].courses,
                            'home_position': coursesByTrack[key].home_position };
                });

                $scope.coursesByTrack.sort(compare_by_position);
            });
        }
    ]);

    app.controller('CourseListController',
        ['$scope', '$window', 'CourseStudent', 'Course', 'UserAccess',
        function ($scope, $window, CourseStudent, Course, UserAccess) {
            // Log this access
            (new UserAccess({ area: 'courses' })).$save().then(
                (success) => {},
                (error) => {
                    console.warn(`The area: 'courses' was not registered in UserAccess`)
                }
            );

            function compare_by_course_student(a,b) {
                if (a.course_student === undefined) {
                    return 1;
                } else  {
                    if (b.course_student !== undefined) {
                        if (b.course_student.last_activity < a.course_student.last_activity)
                            return -1;
                        else
                            return 1;
                    } else {
                        return -1;
                    }
                }
                return 0;
            }

            function compare_by_start_date(a,b) {
                if (a.start_date === null) {
                    return 1;
                } else  {
                    if (b.start_date !== null) {
                        let date_a = a.start_date.replaceAll('-', '')
                        let date_b = b.start_date.replaceAll('-', '')
                        if (date_b < date_a)
                            return -1;
                        else
                            return 1;
                    } else {
                        return -1;
                    }
                }
                return 0;
            }

            function fetchCourses () {
                Course.query({'public_courses': 'True', }, function (courses) {
                    $scope.courses = courses;
                    $scope.recent_courses = []
                    CourseStudent.query({}, function (course_students){
                        $scope.course_students = course_students
                        angular.forEach($scope.courses, function(course) {
                            $scope.langs = []
                            angular.forEach(course.lang, function(lang) {
                                let langs_dict = {
                                    'en': 'English',
                                    'es': 'Spanish',
                                    'pt-br': 'Portuguese'
                                }
                                $scope.langs.push(langs_dict[lang]);
                            });
                            course.lang = $scope.langs
                            angular.forEach($scope.course_students, function(course_student) {
                                if (course.id === course_student.course.id) {
                                    course.course_student = course_student;
                                }
                            });
                        });
                        $scope.courses.sort(compare_by_course_student);
                    });
                    if (courses.length >= 10) {
                        $scope.recent_courses = [...courses];
                        $scope.recent_courses.sort(compare_by_start_date);
                        $scope.recent_courses = $scope.recent_courses.slice(0, 5);
                    }
                });
            }

            fetchCourses();
            $scope.$on('languageChanged', () => {
                fetchCourses();
            });
         }]
    );

    app.controller('UserCourseListController',
        ['$scope', '$window', '$uibModal', 'CertificationProcess', 'CourseStudentService', 'Class',
        function ($scope, $window, $uibModal, CertificationProcess, CourseStudentService, Class) {
            $scope.course_student_list = CourseStudentService.get();
            $window.csl = $scope.course_student_list;
            $window.$scope = $scope;

            $scope.createCertificationProcess = function (cs){
                var cp = new CertificationProcess();
                cs = getResource(cs);
                cp.student = cs.user.id;
                cp.klass = cs.current_class.id;
                if(!cs.certificate)
                    cp.course_certification = null;
                else {
                    cp.course_certification = cs.certificate.link_hash;
                }
                cp.evaluation = null;
                cp.$save(function(new_cp){
                    $('#dashboard-tabs a[href=#my-certificates]').tab('show');
                    cs.certificate.processes = cs.certificate.processes || [];
                    cs.certificate.processes.push(new_cp);
                });
            }

            $scope.hasOpenProcess = function (cs) {
                var course_students = getResource(cs);
                if (course_students && course_students.certificate)
                    return (course_students.certificate.processes.length > 0);
                else
                    return false;
            }

            // FIXME make all app angular
            var getResource = function (cs) {
                for(var i = 0; i < $scope.course_student_list.length; i++){
                    if($scope.course_student_list[i].id == cs)
                        return $scope.course_student_list[i];
                }
                return undefined;
            }
        }
    ]);

    app.controller('UserCertificatesController',
        ['$scope', '$window', '$uibModal', 'CertificationProcess', 'CourseStudentService', 'Class',
        function ($scope, $window, $uibModal, CertificationProcess, CourseStudentService, Class) {
            $scope.course_student_list = CourseStudentService.get();
            console.log($scope.course_student_list);
            $scope.open_evaluation_modal = function(process) {
                var modalInstance = $uibModal.open({
                       templateUrl: 'evaluation_modal.html',
                       controller: EvaluationModalInstanceCtrl,
                       resolve: {
                           process: function () {
                               return process;
                           }
                       }
                });
                modalInstance.result.then(function (process) {
                    console.log($scope.course_student_list);
                });

            };

            var EvaluationModalInstanceCtrl = function($scope, $uibModalInstance, process) {
                $scope.process = process;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss();
                };
            };
        }
    ]);

    app.controller('AssistantCourseListController', [
        '$scope', '$window', '$uibModal', 'Lesson', 'CourseProfessor', 'Class',
        function ($scope, $window, $uibModal, Lesson, CourseProfessor, Class) {
            var current_user_id = parseInt($window.user_id, 10);

            $scope.loadLessons = function(course) {
                if(!course.lessons) {
                    Lesson.query({'course__id': course.id}).$promise
                        .then(function(lessons){
                            course.lessons = lessons;
                        });
                }
            };

            $scope.courses_user_assist = CourseProfessor.query({'user': current_user_id,
                          'role': 'assistant'});

            $scope.courses_user_coordinate = CourseProfessor.query({'user': current_user_id,
                          'role': 'coordinator'});

            $scope.open_professor_modal = function(course_professor) {
                var modalInstance = $uibModal.open({
                       templateUrl: 'create_class_modal.html',
                       controller: CreateClassModalInstanceCtrl,
                       resolve: {
                           course_professor: function () {
                               return course_professor;
                           }
                       }
                });
                modalInstance.result.then(function (course_professor) {
                });
            };

            var CreateClassModalInstanceCtrl = function($scope, $uibModalInstance, course_professor) {
                $scope.course = course_professor.course;

                $scope.cancel = function () {
                    $uibModalInstance.dismiss();
                };
            };
        }
    ]);

    app.controller('CoordinatorCourseListController', [
        '$scope', '$window', '$uibModal', 'Lesson', 'CourseProfessor', 'Class',
        function ($scope, $window, $uibModal, Lesson, CourseProfessor, Class) {
            var current_user_id = parseInt($window.user_id, 10);

            $scope.loadLessons = function(course) {
                if(!course.lessons) {
                    Lesson.query({'course__id': course.id}).$promise
                        .then(function(lessons){
                            course.lessons = lessons;
                        });
                }
            };

            $scope.courses_user_assist = CourseProfessor.query({'user': current_user_id,
                          'role': 'assistant'});

            $scope.courses_user_coordinate = CourseProfessor.query({'user': current_user_id,
                          'role': 'coordinator'});

            $scope.open_professor_modal = function(course_professor) {
                var modalInstance = $uibModal.open({
                       templateUrl: 'create_class_modal.html',
                       controller: CreateClassModalInstanceCtrl,
                       resolve: {
                           course_professor: function () {
                               return course_professor;
                           }
                       }
                });
                modalInstance.result.then(function (course_professor) {
                });
            };

            var CreateClassModalInstanceCtrl = function($scope, $uibModalInstance, course_professor) {
                $scope.course = course_professor.course;

                $scope.cancel = function () {
                    $uibModalInstance.dismiss();
                };
            };
        }
    ]);

})(window.angular);
