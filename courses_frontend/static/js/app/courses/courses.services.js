(function(angular){
    'use strict';

    var app = angular.module('courses.services', []);

    app.factory('CourseBySlug', ['$resource', function($resource){
        return $resource(API_URL + '/course-by-slug/:slug',
            {'slug' : '@slug'},
            {'update': {'method': 'PUT'} });
    }]);

    app.factory('CourseLessons', ['$resource', function($resource){
        return $resource(API_URL + '/course-lessons/:course_id',
            {'course_id' : '@course_id'},
            {'update': {'method': 'PUT'} },
            {'query':  {'method': 'GET', 'isArray': true } });
    }]);

    app.factory('MyCourses', function($resource){
        return $resource(API_URL + '/my-courses', {}, {
        });
    });

    app.factory('CourseStudents', function($resource){
        return $resource(API_URL + '/course_student/', {}, {});
    });

    app.factory('BasicClass', function($resource){
        return $resource(API_URL + '/course_classes_basic/:id', {'id' : '@id'}, {
            'update': {'method': 'PUT'}
        });
    });

    /**
     *  Provide a Course Professor class. The property Class.fields contains the
     *  list of fields that reflects Course model in Django
     */
    app.factory('CourseProfessor', ['$resource', function($resource) {
        return $resource(BASE_API_URL + '/course_professor/:id', {'id':'@id'}, {
            'update': {
                'method': 'PUT'
            }
        });
    }]);

})(angular);
