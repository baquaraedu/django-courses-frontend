(function(angular){
    'use strict';

    var app = angular.module('courses.filters', []);

    app.filter('unitsRatio', function() {
        return function(units_list) {
                if (units_list.length > 20) 
                    return 1
                return 7 - (units_list.length) / 3
        };
    })
})(angular);
