(function() {
    'use strict';

    angular.
    module('courseCard').
    component('courseCard', {
      templateUrl: '/course-card.template.html',
      bindings: {
        course: '<',
      },
    });
})();
