(function(angular){
    'use strict';
    var app = angular.module('forum.controllers', ['ngSanitize']);

    app.controller('ForumCtrl', ['$scope', '$routeParams', '$stateParams', '$http', '$location', '$state', '$dialogs', 'Category', 'Forum', 'ForumPage', 'Tag', 'Topic', 'TopicPage', 'CurrentUser', 'UserAccess', 'gettextCatalog',
        function ($scope, $routeParams, $stateParams, $http, $location, $state, $dialogs, Category, Forum, ForumPage, Tag, Topic, TopicPage, CurrentUser, UserAccess, gettextCatalog) {
            const forum_id = $stateParams.forumId;
            $scope.user = CurrentUser;
            $scope.forum = {}
            $scope.topics = {}
            $scope.list_tags = []
            $scope.search = {txt:""}
            $scope.search_topic = {txt:""}
            // Pagination Params
            $scope.forum_pages_max_number = 10;
            $scope.forum_topics_page = 20;
            $scope.forum.page_size = 10;
            $scope.forum.current_page = 1;
            $scope.load_page = 1;
            $scope.has_forum = true;
            $scope.forum_course = false;

            $scope.ForumCourse = function(forum){
                if (forum) {
                    var forum_current = window.location.pathname.split('/')[4]

                    if (!forum_current) {
                        $scope.has_forum = false;
                    }
                    else {
                        singleInit(forum_current);
                        $scope.forum_course = true;

                    }
                }
            }

            $scope.deleteTopic = function (topic) {
                $dialogs.showConfirmationDialog(
                    gettextCatalog.getString('If you delete this topic it cannot be recovered and comments will be lost!'), {
                        title: gettextCatalog.getString('Delete this topic?'),
                        closable: true,
                        buttonOkText: gettextCatalog.getString('Delete'),
                        callback (option) {
                            if (option === 'ok') {
                                Topic.delete({ id: topic.id }, () => {
                                    $state.reload();
                                });
                            }
                        },
                    }
                );
            }

            if(forum_id) {
                singleInit();
            } else {
                if (!$scope.forum_course) {
                    normalInit();
                }
            }

            function singleInit(id) {
                var forum_current = forum_id;

                if (id) {
                    forum_current = id;
                }

                Forum.get({id: forum_current}, (forum) => {
                    $scope.filters = undefined;
                    $scope.forum_search = false;
                    $scope.forum_single = true;
                    $scope.forums = [];
                    $scope.forum = forum;
                    $scope.topics.current_page = 1;
                    angular.forEach(forum.topics , function(topics) {
                        angular.forEach(topics.tags , function(tags) {
                            $scope.list_tags.push(tags);
                        });
                    });

                    $scope.forum.page = TopicPage.get({
                        page: 1,
                        page_size: $scope.forum_topics_page,
                        forum: forum_current,
                        ordering: '-last_activity_at',
                        course: true},

                        function(page){
                            $scope.forum.topics = page.results;
                            $scope.forum_topics_total = page.count;
                            $scope.topics_loaded = true;
                            $scope.has_next_page = page.next !== null;
                        },
                        function(err){
                            console.log("Erro ao carregar os tópicos");
                        }
                    );
                    $scope.forums.push(forum); // to reuse template's ng-repeat
                },function(err){
                    normalInit();
                });
            }

            function normalInit() {
                // Log this access
                (new UserAccess({ area: 'forums' })).$save().then(
                    (success) => {},
                    (error) => {
                        console.warn("The area: 'forums' was not registered in UserAccess")
                    }
                );

                $scope.filters = {};
                $scope.forum_single = false;
                const categoriesParams = $routeParams['categories'];
                const tagParams = $routeParams['tags']

                if(categoriesParams || tagParams) {
                    if(categoriesParams) {
                        if(typeof categoriesParams === 'string' || typeof categoriesParams === 'number')
                            $scope.filters.categories = [categoriesParams];
                        else
                            $scope.filters.categories = categoriesParams;

                        $scope.filters.categories = $scope.filters.categories.map(function(cat) {
                            return angular.fromJson(cat);
                        });
                    }
                    else {
                        $scope.filters.categories = [];
                    }
                    if(tagParams) {
                        if(typeof tagParams === 'string' || typeof tagParams === 'number')
                            $scope.filters.tags = [tagParams];
                        else
                            $scope.filters.tags = tagParams;
                        $scope.filters.tags = $scope.filters.tags.map(function(tag) {
                            return angular.fromJson(tag);
                        });
                    }
                    else {
                        $scope.filters.tags = [];
                    }
                    $scope.forum_search = true;
                }
                else {
                    $scope.filters.categories = [];
                    $scope.filters.tags = [];
                    $scope.forum_search = false;
                }

                $scope.forums = ForumPage.get({
                    search: $scope.current_search,  // if there is a search in progress, keep it
                    page: $scope.forum.current_page,
                    page_size: $scope.forum.page_size,
                    latest_topics: true,
                }, (response) => {
                    $scope.forums = response.results
                    $scope.forum.total_forum_items = response.count
                    $scope.forum.max_size = response.length
                    $scope.forum_page_loaded = response.$resolved;
                    $scope.forum.has_next_page = (response.next !== null || response.previous !== null)
                });
                $scope.latest_topics = Topic.query({
                    limit: 6,
                    ordering: '-last_activity_at',
                    }, function(){
                        $scope.topics_loaded = true;
                    }
                );
            }

            $scope.filtersChanged = function(type, value){
                if (type === 'clean') {
                    if (value === 'tag')
                        $scope.tags = null;
                    if (value === 'cat')
                        $scope.category = null;
                    if (!value) {
                        $scope.tags = null;
                        $scope.category = null;
                    }
                }

                TopicPage.get({
                    forum: forum_id,
                    search: $scope.search_topic.txt,
                    tag: $scope.tags ? $scope.tags.id : null,
                    category: $scope.category ? $scope.category.id : null,
                    page: 1,
                    page_size: $scope.forum_topics_page,
                    ordering: '-last_activity_at'},
                    function(page){
                        $scope.forum.topics = page.results;
                        $scope.forum_topics_total = page.count;
                        $scope.topics_loaded = true;
                        $scope.has_next_page = page.next !== null;
                });
            };

            $scope.loadMore = () => {
                $scope.load_page += 1;

                TopicPage.get({
                    forum: forum_id,
                    search: $scope.search_topic.txt,
                    tag: $scope.tags ? $scope.tags.id : null,
                    category: $scope.category ? $scope.category.id : null,
                    page: $scope.load_page,
                    page_size: $scope.forum_topics_page,
                    ordering: '-last_activity_at'},
                    function(page){
                        let results = $scope.forum.topics.concat(page.results)
                        $scope.forum.topics = results;
                        $scope.forum_topics_total = page.count;
                        $scope.topics_loaded = true;
                        $scope.has_next_page = page.next !== null;
                });
            };

            $scope.getResultsTopic = function(txt) {
                $scope.current_search = txt;

                TopicPage.get({
                    forum: forum_id,
                    search: txt !== '' ? txt : null,
                    tag: $scope.tags ? $scope.tags.id : null,
                    category: $scope.category ? $scope.category.id : null,
                    page: 1,
                    ordering: '-last_activity_at',
                    page_size: $scope.forum_topics_page,
                    ignoreLoadingBar: true},
                    function(page){
                        $scope.forums.topics = [];
                        $scope.forum.topics = page.results;
                        $scope.forum_topics_total = page.count;
                        $scope.topics_loaded = true;
                        // $scope.forum_search = true;
                        $scope.forum_single = true;
                        $scope.has_next_page = page.next !== null;
                    });
            };

            // Pagination controls
            $scope.topicPageChanged = function(){
              $scope.forum.page = TopicPage.get({
                  search: $scope.current_search,  // if there is a search in progress, keep it
                  page: $scope.topics.current_page,
                  page_size: $scope.forum_topics_page,
                  forum: forum_id,
                  ordering: '-last_activity_at'},
                  function(page){
                      $scope.forum.topics = page.results;
                      $scope.topics_loaded = true;
                  },
                  function(err){
                      console.log("Erro ao carregar os tópicos");
                  }
              );
            };
            $scope.forumPageChanged = () => {
                $scope.forums = ForumPage.get({
                    search: $scope.current_search,  // if there is a search in progress, keep it
                    page: $scope.forum.current_page,
                    page_size: $scope.forum.page_size
                }, (response) => {
                    $scope.forums = response.results
                    $scope.forum.total_forum_items = response.count
                    $scope.forum.max_size = response.length
                    $scope.forum_page_loaded = response.$resolved;
                });
              };

            $scope.getTopicResults = function(txt) {
                $scope.current_search = txt;
                TopicPage.get({
                    search: txt,
                    page: 1,
                    page_size: $scope.forum_topics_page,
                    ordering: '-last_activity_at',
                    ignoreLoadingBar: true},
                    function(page){
                        $scope.forums = [];
                        $scope.forum.title = "Resultados de busca";
                        $scope.topics.current_page = 1;
                        $scope.forum.topics = page.results;
                        $scope.forum_topics_total = page.count;
                        $scope.topics_loaded = true;

                        $scope.filters = undefined;
                        $scope.forum_search = true;
                        $scope.forum_single = false;
                        $scope.forums.push($scope.forum); // to reuse template's ng-repeat
                }, function(err){
                    normalInit();
                });
            }

            $scope.getForumAndTopicResults = function(txt) {
                $scope.current_search = txt;
                $scope.forums = ForumPage.get({
                    search: txt,
                    page: 1,
                    page_size: $scope.forum.page_size,
                }, (response) => {
                    $scope.forums = response.results
                    $scope.forum.total_forum_items = response.count
                    $scope.forum.max_size = response.length
                    $scope.forum_page_loaded = response.$resolved;
                    $scope.forum.has_next_page = (response.next !== null || response.previous !== null)
                });
            }

            function clear_filters() {
                $scope.filters = {};
                $scope.filters.categories = [];
                $scope.filters.tags = [];
            }

            $scope.clear_search = () => {
                $scope.forum_search = false;
                $scope.current_search = "";
                $scope.forums = {}
                $scope.topics_loaded = false;
                $scope.search = {txt:""}
                normalInit()
            }

            function set_route() {
                var new_url = '#!/forum';
                if (forum_id)
                    new_url += forum_id;
                var plain_url = true;
                for(var i = 0; i < $scope.filters.categories.length; i++) {
                    if (plain_url)
                        new_url += '?categories=' + angular.toJson($scope.filters.categories[i]);
                    else
                        new_url += '&categories=' + angular.toJson($scope.filters.categories[i]);
                    plain_url = false;
                }
                for(var i = 0; i < $scope.filters.tags.length; i++) {
                    if (plain_url)
                        new_url += '?tags=' + angular.toJson($scope.filters.tags[i]);
                    else
                        new_url += '&tags=' + angular.toJson($scope.filters.tags[i]);
                    plain_url = false;
                }
                window.location.hash = new_url;
            }

            $scope.forumFilter = function(operation, type, filter_obj) {

                if(!$scope.filters) {
                    $scope.filters = {};
                    $scope.filters.categories = []
                    $scope.filters.tags = []
                }

                if(operation == 'clear') {
                    clear_filters();
                    normalInit();
                    return;
                }

                if(type === 'cat') {
                    if(operation === 'add') {
                        $scope.filters.categories.some(obj => obj.name === filter_obj.name) ?
                            console.log('already filtering by this category') :
                            $scope.filters.categories.push(filter_obj);
                    }
                    else {
                        $scope.filters.categories.splice( $scope.filters.categories.indexOf(filter_obj), 1 );
                    }
                }
                else {
                    if(operation === 'add') {
                        $scope.filters.tags.some(obj => obj.name === filter_obj.name) ?
                            console.log('already filtering by this tag') :
                            $scope.filters.tags.push(filter_obj);
                    }
                    else {
                        $scope.filters.tags.splice( $scope.filters.tags.indexOf(filter_obj), 1 );
                    }
                }

                if($scope.filters.categories.length + $scope.filters.tags.length === 0) {
                    clear_filters();
                }

                set_route();

                $scope.forums = ForumPage.get({
                    search: $scope.current_search,  // if there is a search in progress, keep it
                    page: 1,
                    page_size: $scope.forum.page_size,
                    categories : $scope.filters.categories.map(function(el) {
                        return el.id;
                    }), //array with cat id's
                    tags : $scope.filters.tags.map(function(el) {
                        return el.id;
                    }) //array with tag id's
                }, (response) => {
                    $scope.forums = response.results
                    $scope.forum.total_forum_items = response.count
                    $scope.forum.max_size = response.length
                    $scope.forum_page_loaded = response.$resolved;
                    $scope.forum.has_next_page = (response.next !== null || response.previous !== null)
                    $scope.forum_search = true
                });
            }
        }
    ]);
})(window.angular);
