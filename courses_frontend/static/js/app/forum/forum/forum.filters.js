(function(angular){
    'use strict';
    var app = angular.module('forum.filters', []);

    app.filter('actionFilter',function(){
        return function(txt) { // [ação no] tópico tal
            var filtered;
            switch(txt) {
                case 'new_comment':
                    filtered = 'commented on';
                    break;
                case 'new_topic':
                    filtered = 'created the';
                    break;
                case 'new_reaction':
                    filtered = 'liked the';
                    break;
                case 'new_reaction_comment':
                    filtered = 'did you like a comment on';
                    break;
            }
            return filtered;
        }
    });

    app.filter('dateFilter', ['gettextCatalog', function(gettextCatalog){
        return function(dt) {
            if (dt === undefined){
              return;
            }
            var past = new Date(dt),
                now = new Date(),
                diff = now.getTime() - past.getTime(),
                labels = {
                    //'ano': 31536000000,
                    'month': 2592000000,
                    //'semana': 604800000,
                    'day': 86400000,
                    'hour': 3600000,
                    'minute': 60000,
                    'second': 1000
                },
                time_int,
                filtered = [];
            angular.forEach(labels,function(val,time_unit){
                time_int = Math.floor(diff/val);
                if(diff>=val && time_int > 0) {
                    if(time_int > 1) {
                        time_unit = time_unit+'s';
                    }
                    filtered.push({
                        'time_int':time_int,
                        'time_unit':time_unit
                    });
                }
            });
            if(filtered[0] === undefined) return ""; // prevent runtime error with undefined
            // if(filtered[0].time_unit == "mês" && filtered[0].time_int > 1) {
            //     return "em "+past.getDate()+"/"+(past.getMonth()+1)+"/"+past.getFullYear()+", às "+past.getHours()+":"+past.getMinutes();
            // }
            else {
                return filtered[0].time_int + " " + gettextCatalog.getString(filtered[0].time_unit);
            }
        }
    }]);

    app.filter('charLimiter', ['$filter', function($filter) {
        return function(input, max_length) {
            if(input.length <= max_length){
                // If the input is smaller than the length provided, do nothing
                return input;
            } else {
                // Otherwise, limit the length of the content and add "..."
                return $filter('limitTo')(input, max_length) + "...";
            }
        };
    }]);

})(window.angular);
