(function() {
    'use strict';

    angular.
    module('forum').
    component('forum', {
      templateUrl: '/forum.template.html',
      controller: 'ForumCtrl',
    });
})();
