(function(angular){
    'use strict';

    var app = angular.module('forum', [
        'forum.controllers',
        'forum.services',
        'forum.directives',
        'forum.filters',
        'core.services',
        'ui.router',
        'ui.tinymce',
        'ui.bootstrap',
        'ngFileUpload',
        'ui.select',
        'ngSanitize',
        'ngAnimate',
        // 'duScroll',
        'LocalStorageModule',
        // 'shared',
        'djangular'
    ]);

})(angular);
