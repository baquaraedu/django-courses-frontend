(function() {
    'use strict';
    
    var app = angular.module('categoryEdit', [
        'category-edit.controllers',
        'ui.tinymce',
    ]);

})(angular);