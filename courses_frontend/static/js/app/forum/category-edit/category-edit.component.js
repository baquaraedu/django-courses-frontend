(function() {
    'use strict';

    angular.
    module('categoryEdit').
    component('categoryEdit', {
      templateUrl: '/category-edit.template.html',
      controller: 'CategoryEditCtrl',
    });
})();
