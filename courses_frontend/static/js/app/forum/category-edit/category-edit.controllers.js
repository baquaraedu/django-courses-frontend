(function(angular){
    'use strict';

    var app = angular.module('category-edit.controllers', ['ngSanitize']);
    
    app.controller('CategoryEditCtrl', ['$scope', '$window', '$stateParams', 'Category', 'SetLanguage','UserLocalStorage', 'gettextCatalog',
        function ($scope, $window, $stateParams, Category, SetLanguage, UserLocalStorage, gettextCatalog) {
            $scope.name = "";
            $scope.description = "";
            $scope.alert = {
                hidden: true,
                type: 'success'
            }

            if ($stateParams.categoryId) {
                singleInit();
            }

            function singleInit(category_id) {
                const categoryId = $stateParams.categoryId;

                if (category_id) { categoryId = category_id; }

                Category.get({id: categoryId}, (category) => {
                    $scope.category = category;
                    $scope.name = category.name;
                    $scope.description = category.description;
                }, function(error) {
                    $scope.fatal_error = true;
                    $scope.error_message = error.data.message;
                });
            };

            $scope.editingLanguage = UserLocalStorage.get('editingLanguage') || UserLocalStorage.get('currentLanguage');
            
            $scope.setLanguage = function(language) {
                UserLocalStorage.set('editingLanguage', language);
                $scope.language = language;
                SetLanguage(language).then(function() {
                    $window.location.reload();
                });
            }

            $scope.delete_category = function() {
                let category = new Category();
                category.id = $stateParams.categoryId;
                category.name = $scope.name;
                category.description = $scope.description;

                if(!confirm(gettextCatalog.getString('Are you sure you want to remove this category?'))) return;
                $scope.category.$delete().then(function(){
                    window.location.href = `#!/categories/`;
                    $scope.alert = {
                        title: gettextCatalog.getString('Deleted with success'),
                        hidden: false,
                        type: 'success'
                    }
                });
            }

            $scope.save_category = function() {
                let editingLanguage = $scope.editingLanguage;
                let category = $scope.category ?  $scope.category : new Category();

                category.id = $stateParams.categoryId;
                category.name = $scope.name;
                category.description = $scope.description;

                if (editingLanguage === 'en') {
                    category.name_en = category.name;
                    category.description_en = category.description;
                } else if (editingLanguage === 'es') {
                    category.name_es = category.name;
                    category.description_es = category.description;
                } else if (editingLanguage === 'pt-br') {
                    category.name_pt_br = category.name;
                    category.description_pt_br = category.description;
                }

                if (category.id) {
                    category.$update(category => {
                            $scope.category = category;
                            $scope.alert = {
                                title: gettextCatalog.getString('Updated with success'),
                                hidden: false,
                                type: 'success'
                            }
                        }, function(error) {
                            $scope.fatal_error = true;
                            $scope.alert = {
                                title: error.data.message,
                                hidden: false,
                                type: 'error'
                            }
                        }
                    )
                } else {
                    category.$save(function(category) {
                        window.location.href = `#!/category/${category.id}`;
                        $scope.alert = {
                            title: gettextCatalog.getString('Saved with success'),
                            hidden: false,
                            type: 'success'
                        }
                    })
                }
            }
        }
    ]);

})(window.angular);
