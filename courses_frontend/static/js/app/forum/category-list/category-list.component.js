(function() {
    'use strict';

    angular.
    module('categoryList').
    component('categoryList', {
      templateUrl: '/category-list.template.html',
      controller: 'CategoryListCtrl',
    });
})();
