(function(angular){
    'use strict';

    var app = angular.module('category-list.controllers', ['ngSanitize']);
    
    app.controller('CategoryListCtrl', ['$scope', '$window', '$stateParams', 'CategoryPage', 'UserLocalStorage',
        function ($scope, $window, $stateParams, CategoryPage, UserLocalStorage) {
            UserLocalStorage.set('editingLanguage', UserLocalStorage.get('currentLanguage'));
            $scope.category = {}
            $scope.search = ""
            
            // Pagination Params
            $scope.category.page_size = 20;
            $scope.current_page = 1;
            $scope.load_page = 1;
            $scope.category_page_loaded = false;

            $scope.categories = CategoryPage.get({
                search: $scope.search,
                page: $scope.current_page,
                page_size: $scope.category.page_size,
            }, (response) => {
                $scope.categories = response.results
                $scope.category.total_category_items = response.count
                $scope.category.max_size = response.length
                $scope.category_page_loaded = response.$resolved
                $scope.category.has_next_page = (response.next !== null || response.previous !== null)
            }, (err) => {
                $scope.category_page_loaded = true
                $scope.categories = []
            });
            
            $scope.page_changed = function() {
                $scope.categories = CategoryPage.get({
                    search: $scope.search,
                    page: $scope.current_page,
                    page_size: $scope.category.page_size,
                }, (response) => {
                    $scope.categories = response.results
                    $scope.category.total_category_items = response.count
                    $scope.category.max_size = response.length
                    $scope.category_page_loaded = response.$resolved;
                    $scope.category.has_next_page = (response.next !== null || response.previous !== null)
                }, (err) => {
                    $scope.category_page_loaded = true;
                    $scope.categories = []
                });
            };
        }
    ]);

})(window.angular);
