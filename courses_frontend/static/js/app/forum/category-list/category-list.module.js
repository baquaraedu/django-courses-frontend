(function() {
    'use strict';
    
    var app = angular.module('categoryList', [
        'category-list.controllers',
        'ui.tinymce',
    ]);

})(angular);