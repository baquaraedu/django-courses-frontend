(function() {
    'use strict';

    angular.
    module('topic').
    component('topic', {
      templateUrl: '/topic.template.html',
      controller: 'TopicCtrl',
    });
})();
