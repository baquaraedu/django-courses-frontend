(function() {
    'use strict';
    
    var app = angular.module('topic', [
        'topic.controllers',
        'forum.services',
        'forum.directives',
        'forum.filters',
        'core.services',
        'ui.router',
        'ui.tinymce',
        'ui.bootstrap',
        'ngFileUpload',
        'ui.select',
        'ngSanitize',
        'ngAnimate',
        'LocalStorageModule',
        'djangular'
    ]);

})(angular);