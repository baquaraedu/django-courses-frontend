(function(angular){
    'use strict';

    var app = angular.module('new-topic.controllers', ['ngSanitize']);
    
    app.controller('NewTopicCtrl', ['$scope', '$window', '$location', 'Forum', 'BasicForum', 'Topic', 'TopicFile', 'Category', 'Tag', 'ContentFile', 'CurrentUser',
        function ($scope,  $window, $location, Forum, BasicForum, Topic, TopicFile, Category, Tag, ContentFile, CurrentUser,
        ) {
            $scope.selected_forum = '';
            $scope.forums = BasicForum.query();
            $scope.categories = Category.query();
            $scope.tags = Tag.query();
            $scope.new_topic = new Topic();
            $scope.pinned = false;

            $scope.user = CurrentUser;

//            uiTinymceConfig.images_upload_handler = ContentFile.upload;

            $scope.NewForumCourse = function(forum){
                if (forum) {
                    var forum = window.location.pathname.split('/')[4] 
                    Forum.get({id:forum}, function(t) {
                        $scope.list_categories = t.category;
                        $scope.selected_forum = t;
                        $scope.new_topic.forum = t.id;
                    });
                }   
            }

            $scope.save_topic = function(type) {
                $scope.sending = true;
                $scope.new_topic.forum = $scope.selected_forum.id;
                $scope.new_topic.categories = [$scope.category];
                var topic_files = $scope.new_topic.files;
                $scope.new_topic.$save(function(topic){
                    angular.forEach(topic_files, function(topic_file) {
                        topic_file.topic = topic.id;
                        delete topic_file.file;
                        topic_file.$patch().then(function(comment_file_complete) {
                            topic.files.push(comment_file_complete);
                        });
                    })
                    if (type === 'course') {
                        window.location.href = window.location.pathname.replace("new_topic", "topic/" + topic.id);
                    } else {
                        var url = '#!/topic/'+topic.id;
                        $window.location.href = url;
                    }
                });
            }

            $scope.show_errors = false;
            $scope.validate = function(valid) {
                console.log('VALIDATE', $scope.category);
                $scope.show_errors = true;
                if(!valid) {
                    setTimeout(function() {
                        $('html, body').animate({
                            scrollTop: $('#errors-list').position().top
                        }, 500);
                    }, 100);
                }
            }

            // Turn new tags into serializable objects
            $scope.tagTransform = function (newTag) {
                var item = {
                    name: newTag.toLowerCase()
                    };
                    return item;
            };

            $scope.new_topic.tags = [];
            $scope.tagExists = function (newTag) {
                for (var tag of $scope.tags)
                    if (tag.name.toLowerCase() == newTag)
                        return true;
                return false;
            }

            $scope.filter_categories = function(){
                $scope.list_categories = $scope.selected_forum.category;
                let filteredGroups = $scope.selected_forum.groups.filter(value =>  $scope.user.groups_ids.includes(value));

                if(filteredGroups.length > 0) {
                    $scope.pinned = true;
                } else {
                    $scope.pinned = false;
                } 
            }

            $scope.uploadTopicFiles = function (file, topic) {

                if (file) {
                    TopicFile.upload(file).then(function (response) {
                        var comment_file = new TopicFile(response.data);

                        if (topic.files === undefined)
                            topic.files = [];
                        topic.files.push(comment_file);
                        return {location: comment_file.file};
                    }, function (response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    }, function (evt) {
                        topic.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
            };
        }
    ]);

})(window.angular);
