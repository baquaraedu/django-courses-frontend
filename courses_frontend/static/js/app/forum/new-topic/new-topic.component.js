(function() {
    'use strict';

    angular.
    module('new-topic').
    component('new-topic', {
      templateUrl: '/new-topic.template.html',
      controller: 'NewTopicCtrl',
    });
})();
