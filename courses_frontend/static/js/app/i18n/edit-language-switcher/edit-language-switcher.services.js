class EditLanguageSwitcherService {

    static get $inject () {
        return [];
    }

    _populateModel (model, structure, from, to) {
        for (const path of structure) {
            if (typeof path === 'string') {
                model[path + to] = model[path + from];
            } else {
                for (const [key, value] of Object.entries(path)) {
                    this._populateModel(model[key], value, from, to);
                }
            }
        }
    }

    /**
     * Copy from localized model to base model
     */
    fromLocale (model, structure, locale) {
        if (model.$promise) {
            model.$promise.then((resolvedModel) => {
                this._populateModel(resolvedModel, structure, `_${locale}`, '');
            });
        } else {
            this._populateModel(model, structure, `_${locale}`, '');
        }
    }

    /**
     * Copy from base model to localized model
     */
    saveLocale (model, structure, locale) {
        if (model.$promise) {
            model.$promise.then((resolvedModel) => {
                this._populateModel(resolvedModel, structure, '', `_${locale}`);
            });
        } else {
            this._populateModel(model, structure, '', `_${locale}`);
        }
    }
}

angular.module('editLanguageSwitcher')
    .factory('EditLanguageSwitcherService', EditLanguageSwitcherService);
