class EditLanguageSwitcherController {

    static get $inject () {
        return [
            '$state',
            'EditLanguageSwitcherService',
            'TranslationService',
        ];
    }

    constructor ($state, EditLanguageSwitcherService, TranslationService) {
        this.$state = $state;
        this.EditLanguageSwitcherService = EditLanguageSwitcherService;
        this.TranslationService = TranslationService;
    }

    $onInit () {
        this.availableLanguages = this.TranslationService.getAvailableLanguages();
        this.language = this.TranslationService.currentLanguage;

        this.EditLanguageSwitcherService.fromLocale(this.model, this.structure, this.language);

        if (this.availableLanguages.length === 1 && this.language !== this.availableLanguages[0].code) {
            this.setLanguage(this.availableLanguages[0].code);
        }
    }

    setLanguage (language) {
        const oldLanguage = this.language;
        this.language = language;

        this.EditLanguageSwitcherService.saveLocale(this.model, this.structure, oldLanguage);
        this.EditLanguageSwitcherService.fromLocale(this.model, this.structure, this.language);
    }
}

angular.module('editLanguageSwitcher')
    .component('editLanguageSwitcher', {
            templateUrl: '/edit-language-switcher.template.html',
            controller: EditLanguageSwitcherController,
            bindings: {
                language: '=',
                model: '<',
                structure: '<',
            },
    });
