var module = angular.module('i18n.services', [
    'i18n.services',
    'tmh.dynamicLocale',
]);

module.config(function(tmhDynamicLocaleProvider) {
    tmhDynamicLocaleProvider.localeLocationPattern('https://unpkg.com/angular-i18n@1.8/angular-locale_{{locale}}.js');
  });

export class TranslationService {
    constructor(
        $http,
        $window,
        $rootScope,
        gettextCatalog,
        UserLocalStorage,
        tmhDynamicLocale,
    ) {
        this.$http = $http;
        this.$window = $window;
        this.$rootScope = $rootScope;
        this.gettextCatalog = gettextCatalog;
        this.UserLocalStorage = UserLocalStorage;
        this.tmhDynamicLocale = tmhDynamicLocale;

        // TODO get these from API/backend
        this.__defaultLanguage = 'en';
        this.__availableLanguages = [
            {
                name: 'English',
                code: 'en',
                short_code: 'en',
                cap_short_code: 'En'
            },
            {
                name: 'Español',
                code: 'es',
                short_code: 'es',
                cap_short_code: 'Es'
            },
            {
                name: 'Português',
                code: 'pt_br',
                short_code: 'pt',
                cap_short_code: 'Pt'
            },
        ];

        this.currentLanguage = this.getLanguage();
        this.setLanguage(this.currentLanguage, false, true);

        // The line above is for dev porpouses
        // uncomment it to show untranslated string on frontend
        // if you commit this, you must pay all the project team a beer, be ware!!!
        // gettextCatalog.debug = true;

    }

    getLanguage() {

        let language = this.getUserLanguage();
        if (!language)
            language = this.getShortLanguage(this.getBrowserLanguage());
        if (!language)
            language = this.getDefaultLanguage();
        return language;
    }

    getBrowserLanguage() {
        // TODO iterate over $window.navigator.languages if the prefered
        // language is not on available languages
        let lang = this.$window.navigator.language;
        if (lang)
            return lang;
        else {
            this.$window.navigator.languages ? this.$window.navigator.languages[0] : null;
            lang = lang || this.$window.navigator.language || this.$window.navigator.browserLanguage || this.$window.navigator.userLanguage;

            return lang;
        }
    }

    getShortLanguage(language) {
        // TODO Refactor this mess!!! the only need for the wrong format with _ is the file names after
        // refactor all apps and moving the translation strings to the json files
        language = language.replace('_', '-').split('-')[0];
        return (language === 'pt') ? 'pt_br' : language;
    }

    getDefaultLanguage() {
        // TODO get this from API/backend
        return this.__defaultLanguage;
    }

    setDefaultLanguage(language) {
        this.__defaultLanguage = language;
    }

    getUserLanguage() {
        return this.UserLocalStorage.get('currentLanguage');
    }

    setLanguage(language, save=false, init=false) {

        let eventData = {
            newLanguage: language,
            oldLanguage: this.currentLanguage,
        }

        // Set language at Angular Gettext
        this.gettextCatalog.setCurrentLanguage(language);
        this.gettextCatalog.loadRemote("/static/js/app/i18n/languages/" + language + ".json");
        if (save) {
            // Set language at User Local Storage
            this.UserLocalStorage.set('currentLanguage', language);
        }

        // TODO generalize this for all languages
        language = (language === 'pt_br'?  'pt-br' : language);

        // Set angular locale
        this.tmhDynamicLocale.set(language);

        // Set language at django backend and return the promise
        let setBackendLanguagePromise = this.setBackendLanguage(language);
        setBackendLanguagePromise.then(() => {
            if (!init) {
                // Avoid these events being triggered when loading
                // the app for the first time.
                this.reloadPage(eventData);
                this.$rootScope.$broadcast('languageChanged', eventData);
            }
        });
        return setBackendLanguagePromise;
    }

    setBackendLanguage(language){
        return this.$http({
            method:'POST',
            url: API_URL + '/i18n/setlang/',
            data: 'language=' + language,
            headers:{'Content-Type':'application/x-www-form-urlencoded'}
        });
    };

    getAvailableLanguages() {
        // TODO get this from API/backend
        return this.__availableLanguages;
    }

    setAvailableLanguages(languages) {
        this.__availableLanguages = languages;
    }

    shouldReloadPath(path) {
        const paths = [
            '/admin/courses/',
            '/admin/course/',
            '/course/',
            '/legacy/admin/courses/',
            '/legacy/admin/users/',
            '/profile/edit',
            '/accounts/signup',
            '/accounts/confirm-email'
        ];

        return paths.some((fragment) => {
            return (fragment instanceof RegExp) ? fragment.test(path) : path.startsWith(fragment);
        });
    }

    shouldReloadHash(hash) {
        const hashes = [
            '#!/categories',
            '#!/category/',
            '#!/format/',
            '#!/formats',
            '#!/profile',
            '#!/reports',
            '#!/theme/',
            '#!/themes',
        ];

        return hashes.some((fragment) => {
            return (fragment instanceof RegExp) ? fragment.test(hash) : hash.startsWith(fragment);
        });
    }

    reloadPage(event) {
        if (event.newLanguage === event.oldLanguage) {
            return false;
        }

        if (this.shouldReloadPath(this.$window.location.pathname) ||
            this.shouldReloadHash(this.$window.location.hash)) {
            this.$window.location.reload();
            return true;
        }

        return false;
    }
}
TranslationService.$inject = [
    '$http',
    '$window',
    '$rootScope',
    'gettextCatalog',
    'UserLocalStorage',
    'tmhDynamicLocale',
]
module.service('TranslationService', TranslationService);
